<?php
$file = $_SERVER["SCRIPT_NAME"];
$break = Explode('/', $file);
$pfile = $break[count($break) - 1];
?>
<div id="attendees-chat"></div>
<!--<div id="attendees-email"></div>
-->
<div id="auditoriums" class="scroll popup-dialog">
    <div class="heading">
        <h4>Select Auditorum</h4>
    </div>
    <div class="popup-content">
        <ul class="list-unstyled">
            <li><a href="auditorium-01.php">Auditorium 01</a></li>
            <li><a href="auditorium-02.php">Auditorium 02</a></li>
            <li><a href="auditorium-03.php">Auditorium 03</a></li>
        </ul>
    </div>
</div>

<div id="talktous" class="scroll popup-dialog">
    <div class="popup-content">
        <div id="chat_team" class="team_chat_box">
            <div class="chat_history scroll" data-touser="team" id="chat_history_team"></div>
            <form>
                <div class="form-group">
                    <input name="chat_message_team" id="chat_message_team" rows="1" class="input sendmsg" autocomplete="off">
                </div>
                <div class="form-group text-left">
                    <button type="button" name="send_teamchat" class="send_teamchat btn-sendmsg" data-src="<?php echo $pfile ?>" data-to="team" data-from="<?php echo $_SESSION['user_id']; ?>">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="agenda" class="scroll popup-dialog">
    <a class="agenda_close popup-close" href="#"><i class="fas fa-times"></i></a>
    <div class="heading">
        <h4>Conference Agenda</h4>
    </div>
    <div class="popup-content">
        <iframe src="resources/conf-agenda.pdf#toolbar=0" frameborder="0" scrolling="no"></iframe>
    </div>
</div>

<div id="attendees_list" class="scroll popup-dialog">
    <div class="top-area">
        <form method="post">
            <input type="text" id="attendee-search" class="input"><button type="submit" id="search-attendee" value="Search">Search</button><button type="submit" id="clear-search-attendee" value="Clear">Clear</button>
        </form>
        <button type="button" id="refresh-attendees"><i class="fas fa-sync"></i></button>
    </div>
    <div id="attendees"></div>
</div>

<div id="my_briefcase" class="scroll popup-dialog">
    <div class="br-tabs">
        <a href="#" id="tab-chat" onClick="showChats()" class="active">Chat Inbox</a><a href="#" id="tab-dl" onClick="showDownloads()" class="">Downloads</a><a href="#" id="tab-vid" onClick="showVideos()" class="">Videos</a>
    </div>
    <div id="briefcase-inbox" style="display:block;">
        <div id="attendees-list-chat" class="scroll">
        </div>
    </div>
    <div id="briefcase-downloads" style="display:none;">
        <div id="downloads-briefcase" class="scroll">
        </div>
    </div>
    <div id="briefcase-videos" style="display:none;">
        <div id="videos-briefcase" class="scroll">

        </div>
    </div>
</div>