<?php
	require_once "../controls/sesAdminCheck.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Webcast Sessions</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item ">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="sessions.php">Webcast Sessions</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="exhibitors.php">Exhibitors</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="polls.php">Polls</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="feedbacks.php">Feedbacks</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div>
</nav>

<div class="container-fluid bg-white color-grey">
     
    <!--<div class="row mt-1">
        <div class="col-12">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>-->
    <div class="row mt-1 p-2">
        <div class="col-12">
            <a class="btn-add" href="addsession.php">Add Session</a>
        </div>
    </div>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <div id="ses-message"></div>
            <div id="sessions"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getSessions();
});

function getSessions()
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getsessions'},
        type: 'post',
        success: function(response) {
            
            $("#sessions").html(response);
            
        }
    });
    
}

function updateLiveStatus(id, val)
{
    $.ajax({
        url: 'ajax.php',
         data: {action: 'updatelivestatus', sesid:id, val: val },
         type: 'post',
         success: function(output) {
             console.log(output);
             getSessions();
         }
   });   
}


function updEntry(id)
{
    var sesID = '#entry'+id;
    var value = $(sesID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'updateentrystatus', sesId :id, val: value},
        success: function(data){
            if(data == "succ")
            {
                $("#ses-message").html('Entry status updated succesfully').removeClass().addClass('alert alert-success alert-msg').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#ses-message").html('Entry status could not be updated.').removeClass().addClass('alert alert-danger alert-msg').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function updSession(id)
{
    var sesID = '#session'+id;
    var value = $(sesID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'updatesessionstatus', sesId :id, val: value},
        success: function(data){
           // console.log(data);
            if(data == "succ")
            {
                $("#ses-message").html('Session status updated succesfully').removeClass().addClass('alert alert-success alert-msg').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#ses-message").html('Session status could not be updated.').removeClass().addClass('alert alert-danger alert-msg').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function updShow(id)
{
    var sesID = '#show'+id;
    var value = $(sesID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'updateshowstatus', sesId :id, val: value},
        success: function(data){
           // console.log(data);
            if(data == "succ")
            {
                $("#ses-message").html('Show status updated succesfully').removeClass().addClass('alert alert-success alert-msg').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#ses-message").html('Show status could not be updated.').removeClass().addClass('alert alert-danger alert-msg').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function delSes(id)
{
    if(confirm('Are you sure?'))
    {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {action: 'delsession', sesId :id},
            success: function(data){
               // console.log(data);
                if(data == "succ")
                {
                    $("#ses-message").html('Session deleted.').removeClass().addClass('alert alert-success alert-msg').fadeIn().delay(2000).fadeOut();
                    getSessions();
                }
                else                    
                {
                    $("#ses-message").html('Session could not be deleted.').removeClass().addClass('alert alert-danger alert-msg').fadeIn().delay(2000).fadeOut();
                }
    
                
            }
        });
    }
    
}
</script>

</body>
</html>