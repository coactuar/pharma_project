<?php
	require_once "../controls/sesAdminCheck.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Polls</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="sessions.php">Webcast Sessions</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="exhibitors.php">Exhibitors</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="polls.php">Polls</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="feedbacks.php">Feedbacks</a>
        </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div>
</nav>

<div class="container-fluid bg-white color-grey">
     
    <!--<div class="row mt-1">
        <div class="col-12">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>-->
    <div class="row mt-1 p-2">
        <div class="col-12">
            <a class="btn-add" href="addpoll.php">Add Poll Question</a>
        </div>
    </div>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <div id="message"></div>
            <div id="polls"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getPolls();
});


function getPolls()
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpolls'},
        type: 'post',
        success: function(response) {
            
            $("#polls").html(response);
            
        }
    });
    
}

function delPoll(id)
{
    if(confirm('Are you sure?')){
          $.ajax({
              url: 'ajax.php',
               data: {action: 'delpoll', poll: id },
               type: 'post',
               success: function(output) {
                   //alert(output);
                   getPolls();
               }
         });  
    }
}
function updatePoll(pollid, sessid, val)
{
    $.ajax({
        url: 'ajax.php',
         data: {action: 'updatepoll', pollId:pollid, sessId: sessid, val: val },
         type: 'post',
         success: function(output) {
             //alert(output);
             getPolls();
         }
   });   
}

function endPoll(pollid, val)
{
     if(confirm('Are you sure?')){
        $.ajax({
            url: 'ajax.php',
             data: {action: 'endpoll', pollId:pollid, val: val },
             type: 'post',
             success: function(output) {
                 //alert(output);
                 getPolls();
             }
       });  
     }
}

function updatePollRes(pollid,sessid, val)
{
    $.ajax({
        url: 'ajax.php',
         data: {action: 'updatepollres', pollId:pollid, sessId: sessid,val: val },
         type: 'post',
         success: function(output) {
             //alert(output);
             getPolls();
         }
   });   
}
</script>

</body>
</html>