<?php
require_once "../controls/sesAdminCheck.php";
require_once "../functions.php";

$errors = [];
$succ = false;

$exhib_name='';

if(isset($_POST['addexhib-btn'])){
    
  if (empty($_POST['exhibname'])) {
        $errors['exhib'] = 'Exhibitor Name is required';
  }
    
  $exhib_name = $_POST['exhibname'];
  
  
  if(count($errors) == 0){  
    $hall = new Hall();
    $addHall = $hall->addHall();
    if($addHall > 0){
        $succ = true;
    }
  }
}
?>
<!doctype html>
<html> 
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add Exhibitor</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item ">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="sessions.php">Webcast Sessions</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="exhibitors.php">Exhibitors</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="polls.php">Polls</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div>
</nav>
<div class="container-fluid bg-white color-grey">
   
   <div class="row mt-2">
        <div class="col-12 col-md-8 offset-md-2">
            <h6>Add Exhibitor</h6>
            <?php
                if (count($errors) > 0): ?>
                <div class="alert alert-danger">
                  <ul>
                  <?php foreach ($errors as $error): ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                  <?php endforeach;?>
                  </ul>
                </div>
              <?php endif;
              ?>
              <?php
                if ($succ){ 
              ?>
                <div id="registration-confirmation">
                      <div class="alert alert-success">
                      Exhibitor has been added successfully!
                      </div>
                      
                  <a href="exhibitors.php">Continue to Exhibitors List</a> 
                  </div>
                
              <?php 
                }
                else{
              ?>
            <div id="exh-message"></div>
            <form id="add-exhib" method="post" action="">
              
              <div class="form-group">
                <label for="pollques">Exhibitor's Name<sup class="req">*</sup></label>
                <input class="input" id="exhibname" name="exhibname" required>
              </div>
              
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                    <sup class="req">*</sup> marked fields are reqqired.<br>
                        <label for="">&nbsp;</label>
                      <input type="submit" name="addexhib-btn" id="submit" class="form-submit btn-submit" value="Add Exhibitor">
                    </div>
                </div>
              </div>
            </form>
            <?php } ?>
        </div>
   </div>
    
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>