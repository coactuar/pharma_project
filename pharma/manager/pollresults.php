<?php
	require_once "../controls/sesAdminCheck.php";
    require_once "../functions.php";
    
    $poll_id = 0;
    $poll = new Poll();
    if(isset($_GET['id'])){
        $poll_id = $_GET['id'];
        $valid = $poll->isValidPoll($poll_id);
        
        if(!$valid)
        {
            header('location: polls.php');
        }
    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Poll Results</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="sessions.php">Webcast Sessions</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="polls.php">Polls</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div>
</nav>

<div class="container-fluid bg-white color-grey">
     
    <?php
    $curr_poll = $poll->getPoll($poll_id);
//    var_dump($curr_poll);
    ?>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <?php if(!$curr_poll[0]['poll_over'])
                                {
                                    ?>
            <a href="#" onClick="updatePoll('<?php echo $curr_poll[0]['poll_id']; ?>','<?php echo $curr_poll[0]['session_id']; ?>','<?php echo $curr_poll[0]['active']; ?>')" class="btn btn-sm <?php if ($curr_poll[0]['active'] == '1') { echo 'btn-danger'; } else echo 'btn-success';  ?> mb-1">
            <?php if ($curr_poll[0]['active'] == '1') { echo 'Deactivate'; } else echo 'Activate';  ?>
            Poll</a> 
            <a href="#" onClick="endPoll('<?php echo $curr_poll[0]['poll_id']; ?>','1')" class="btn btn-sm btn-danger mb-1">End Poll</a> 
            <?php
                                }
                                else{
            ?>                        
            <a href="#" onClick="endPoll('<?php echo $curr_poll[0]['poll_id']; ?>','0')" class="btn btn-sm btn-success mb-1">Enable Poll</a> 
            <?php        
                                }
             ?>                   
        </div>
    </div>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <div id="message"></div>
            <div id="pollresults"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getPollResult();
});


function getPollResult()
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollresults', pid: '<?php echo $poll_id; ?>'},
        type: 'post',
        success: function(response) {
            console.log(response);
            $("#pollresults").html(response);
            setTimeout(function(){ getPollResult(); }, 10000);
            
        }
    });
}

function updatePoll(pollid, sessid, val)
{
    $.ajax({
        url: 'ajax.php',
         data: {action: 'updatepoll', pollId:pollid, sessId: sessid, val: val },
         type: 'post',
         success: function(output) {
             //alert(output);
             location.reload();

         }
   });   
}

function endPoll(pollid, val)
{
     if(confirm('Are you sure?')){
        $.ajax({
            url: 'ajax.php',
             data: {action: 'endpoll', pollId:pollid, val: val },
             type: 'post',
             success: function(output) {
                 //alert(output);
                 location.href = "polls.php";
             }
       });  
     }
}
</script>

</body>
</html>