<?php
require_once "../controls/sesAdminCheck.php";
require_once "../functions.php";

$exhibitor_id = 0;
$hall = new Hall();
if (isset($_GET['h'])) {
    $exhibitor_id = $_GET['h'];
    $valid = $hall->verifyHallId($exhibitor_id);

    if (!$valid) {
        header('location: exhibitors.php');
    }
} else {
    header('location: exhibitors.php');
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Exhibitor Videos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
    <nav class="navbar navbar-expand-md bg-light">
        <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="dashboard.php">Dashboard</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="users.php">Registered Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sessions.php">Webcast Sessions</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="exhibitors.php">Exhibitors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="polls.php">Polls</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?action=logout">Logout</a>
                </li>
            </ul>

        </div>
    </nav>

    <div class="container-fluid bg-white color-grey">

        <div class="row mt-1 p-2">
            <div class="col-12">
                <?php
                $hall = new Hall();
                $hall_name = $hall->getHall($exhibitor_id);
                echo '<h4>' . $hall_name[0]['exhibitor_name'] . '</h4>';
                ?>
            </div>
        </div>
        <div class="row mt-1 p-2">
            <div class="col-12">
                <div id="exh-message"></div>
                <div id="exhibitors_videos">
                    <?php
                    $hall_videos = $hall->getExhVideoViews($exhibitor_id);
                    //var_dump($hall_videos);
                    if (!empty($hall_videos)) {
                    ?>
                        <table class="table table-striped table-dark">
                            <thead>
                                <tr>
                                    <th>Video Title</th>
                                    <th width="150">No. of Views</th>
                                </tr>
                            </thead>
                            <?php
                            foreach ($hall_videos as $video) {

                                $videoTitle = $hall->getExhVideo($video['video_id']);
                            ?>
                                <tr>
                                    <td>
                                        <?php echo $videoTitle[0]['video_title']; ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo $video['count'];
                                        ?>
                                    </td>

                                </tr>
                            <?php
                            }
                            ?>
                        </table>
                    <?php

                    } else {
                        echo "You have had no views.";
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>


    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>


</body>

</html>