<?php
require_once "../controls/config.php";
require_once "../functions.php";
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
  <nav class="navbar navbar-expand-md bg-light">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    
  </nav>

  <div class="container-fluid bg-white color-grey">

    <div class="row mt-1 p-2">
      <div class="col-12 col-md-4 offset-md-1">
        <h6>Registered Users</h6>
        <?php
        $members = new Admin();
        $reg_users = $members->getMemberCount();
        echo 'Total Registered Users: ' . $reg_users . '<br>';
        $visitors = $members->getVisitorCount();
        echo 'Total Visitors: ' . $visitors . '<br>';
        ?>
      </div>
      <div class="col-12 col-md-4 offset-md-1">
        <h6>Time Spent </h6>
        <?php
        $halls = new Admin();
        $time_spent = $halls->getTotalTimeSpent();
        echo 'Total Time Spent: ' . secToHR($time_spent) . '<br>';
        $avg = $time_spent / $visitors;
        echo 'Avg. Time Spent: ' . secToHR($avg) . '<br>';
        
        ?><br>
      <a href="uservisits.php" target="_blank">Login Details</a> </div>
    </div>
    <div class="row mt-1 p-2">
      <div class="col-12 col-md-4 offset-md-1">
        <h6>Resources</h6>
        <?php
        $halls = new Admin();
        $res = $halls->getResCount();
        echo 'Total Resources: ' . $res . '<br>';
        $res_dl = $halls->getResDLCount();
        $total_dl = 0;
        if (!empty($res_dl)) {
          $total_dl = $res_dl[0]['total'];
        }
        echo 'Total Downloads: ' . $total_dl . '<br>';

        $res_dl = $halls->getResDLList(10);
        if (!empty($res_dl)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($res_dl as $res) {
            ?>
              <tr>
                <td><?php echo '<b>' . $res['exhibitor_name'] . '</b> - ' . $res['resource_title']; ?></td>
                <td><?php echo $res['download_count']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>


      </div>
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Videos</h6>
        <?php
        $halls = new Admin();
        $vids = $halls->getVidCount();
        echo 'Total Videos: ' . $vids . '<br>';
        $vids_views = $halls->getVidViewsCount();
        echo 'Total Views: ' . $vids_views . '<br>';

        $vid_views = $halls->getVidViewsList(10);
        //var_dump($vid_views);
        if (!empty($vids_views)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($vid_views as $vid) {
              //echo $vid['video_id'];
              $videos = new Hall();
              $vidDet = $videos->getExhibVideoDetails($vid['video_id']);
              //var_dump($vidDet);

            ?>
              <tr>
                <td><?php

                    echo '<b>' . $vidDet[0]['exhibitor_name'] . '</b> - ' . $vidDet[0]['video_title'];
                    ?></td>
                <td><?php echo $vid['count']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>

      </div>
    </div>

    <div class="row mt-3">
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Sessions Attendee Count</h6>
        <?php
        $sessions = new Admin();
        $ses_cnt = $sessions->getSessionsCount();
        echo 'Total Sessions: ' . $ses_cnt . '<br>';

        $ses_attcnt = $sessions->getSessionAttendeeCount();
        if (!empty($ses_attcnt)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($ses_attcnt as $sess) {
              $session = new Session();
              $ses_title = $session->getWebcastSessionTitle($sess['session_id']);
            ?>
              <tr>
                <td><?php echo $ses_title; ?></td>
                <td><?php echo $sess['cnt']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>

      </div>
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Sessions Attended</h6>
        <?php
        $sessions = new Admin();
        $ses_att = $sessions->getSessionAttended();
        echo 'Total Session Attendees: ' . $ses_att . '<br>';

        $attendees = $sessions->getSessionAttendees();
        if (!empty($attendees)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($attendees as $attendee) {
              $members = new User();
              $user = $members->getMember($attendee['user_id']);
            ?>
              <tr>
                <td><?php echo $user[0]['first_name'] . ' ' . $user[0]['last_name']; ?></td>
                <td><?php echo $attendee['cnt']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php

        }

        ?>

      </div>
    </div>

    <div class="row mt-3">
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Exhibitor Visits</h6>
        <?php
        $halls = new Hall();
        $all_visitors = $halls->getExhVisitorsCount();
        echo 'Total Exhibitor Visitors: ' . $all_visitors . '<br>';

        $visitors_list = $halls->getVisitorsCountByExh();
        if (!empty($visitors_list)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($visitors_list as $visitors) {
            ?>
              <tr>
                <td><?php
                    $exh_name = $halls->getHall($visitors['exhibitor_id']);
                    echo $exh_name[0]['exhibitor_name'];
                    ?></td>
                <td><?php echo $visitors['cnt']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>
        </table>

      </div>
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Exhibitor Visits by Attendees</h6>
        <?php
        $halls = new Hall();
        $all_visitors = $halls->getExhVisitorsCount();
        echo 'Total Exhibitor Visitors: ' . $all_visitors . '<br>';

        $visitors_list = $halls->getVisitorsExhVisits();
        if (!empty($visitors_list)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($visitors_list as $visitors) {
            ?>
              <tr>
                <td><?php
                    $members = new User();
                    $user_name = $members->getMemberName($visitors['user_id']);
                    echo $user_name;
                    ?></td>
                <td><?php echo $visitors['cnt']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>

      </div>
    </div>

    <div class="row mt-3">
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Time Spent by Visitors at Halls</h6>
        <?php
        $halls = new Hall();
        $total_exhtime = $halls->getExhTimeSpent();
        echo 'Total Time Spent at Exhibit Halls: ' . tosecs($total_exhtime) . '<br>';

        $time_spent = $halls->getExhVisitorTimeSpent();
        if (!empty($time_spent)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($time_spent as $visitor) {
            ?>
              <tr>
                <td><?php
                    $members = new User();
                    $user_name = $members->getMemberName($visitor['user_id']);
                    echo $user_name;
                    ?></td>
                <td><?php echo tosecs($visitor['total']); ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }

        ?>

      </div>
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Exhibitor Visits by Time Spent</h6>
        <?php
        $halls = new Hall();
        $total_exhtime = $halls->getExhTimeSpent();
        echo 'Total Time Spent at Exhibit Halls: ' . tosecs($total_exhtime) . '<br>';

        $time_spent = $halls->getExhTotalTimeSpent();
        if (!empty($time_spent)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($time_spent as $visitor) {
            ?>
              <tr>
                <td><?php
                    $exh_name = $halls->getHall($visitor['exhibitor_id']);
                    echo $exh_name[0]['exhibitor_name'];
                    ?></td>
                <td><?php echo tosecs($visitor['total']); ?></td>
                <td width="50"><a style="color:#fff;" href="exhvisits.php?e=<?php echo $visitor['exhibitor_id']; ?>"><i class="fas fa-download"></i></a></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>
        </table>


      </div>
    </div>
  </div>

<?php
function secToHR($seconds) {
  //$days = floor($seconds /   
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60);
  $seconds = $seconds % 60;
  return $hours > 0 ? "$hours hours, $minutes minutes" : ($minutes > 0 ? "$minutes minutes, $seconds seconds" : "$seconds seconds");//
}
?>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  
</body>

</html>