<?php
require_once "../controls/config.php";
$exh_id = $_GET['e'];
$sql = "select exhibitor_name from tbl_exhibitors where exhibitor_id='".$exh_id."'";
$rs = mysqli_query($link, $sql);
$d = mysqli_fetch_assoc($rs);
$title = $d['exhibitor_name'];


$sql = "SELECT first_name, last_name, emailid, entry_time, exit_time FROM `tbl_exhibitor_visitors`, tbl_users where tbl_exhibitor_visitors.user_id = tbl_users.userid and tbl_exhibitor_visitors.exhibitor_id='".$exh_id."'";
$rs = mysqli_query($link, $sql);
$data = array();
if (mysqli_affected_rows($link) > 0) {
  $i = 0;
  while ($c = mysqli_fetch_assoc($rs)) {
    $data[$i]['First Name'] = $c['first_name'];
    $data[$i]['Last Name'] = $c['last_name'];
    $data[$i]['E-mail ID'] = $c['emailid'];
    $data[$i]['Entry Time'] = $c['entry_time'];
    $data[$i]['Exit Time'] = $c['exit_time'];

    $i++;
  }

  $filename = $title."_Visits.xls";
  header("Content-Type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=\"$filename\"");
  ExportFile($data);
}

function ExportFile($records)
{
  $heading = false;
  if (!empty($records))
    foreach ($records as $row) {
      if (!$heading) {
        // display field/column names as a first row
        echo implode("\t", array_keys($row)) . "\n";
        $heading = true;
      }
      echo implode("\t", array_values($row)) . "\n";
    }
  exit;
}
