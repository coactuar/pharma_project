<?php
require_once "../controls/config.php";


$sql = "SELECT first_name, last_name, emailid, join_time, leave_time FROM `tbl_user_logins`, tbl_users where tbl_user_logins.user_id=tbl_users.userid";
$rs = mysqli_query($link, $sql);
$data = array();
if (mysqli_affected_rows($link) > 0) {
  $i = 0;
  while ($c = mysqli_fetch_assoc($rs)) {
    $data[$i]['First Name'] = $c['first_name'];
    $data[$i]['Last Name'] = $c['last_name'];
    $data[$i]['E-mail ID'] = $c['emailid'];
    $data[$i]['Login Time'] = $c['join_time'];
    $data[$i]['Logout Time'] = $c['leave_time'];

    $i++;
  }

  $filename = "UserVisits.xls";
  header("Content-Type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=\"$filename\"");
  ExportFile($data);
}

function ExportFile($records)
{
  $heading = false;
  if (!empty($records))
    foreach ($records as $row) {
      if (!$heading) {
        // display field/column names as a first row
        echo implode("\t", array_keys($row)) . "\n";
        $heading = true;
      }
      echo implode("\t", array_values($row)) . "\n";
    }
  exit;
}
