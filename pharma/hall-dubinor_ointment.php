<?php
require_once "controls/sesCheck.php";
require_once "functions.php";

$exhibit_hall_id = 'dubinor_ointment';
$exhibitor_id = '19c3dccb03409eb14b5f53dadc7feedb607d7510cee2dad6';

$halls = new Hall();
$verify = $halls->verifyHallId($exhibitor_id);
if (empty($verify)) {
    header('location: exhibition-halls.php');
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Dubinor Ointment :: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

</head>

<body>

    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <div class="navbar-collapse">

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>

        <div class="page-wrapper">
            <div id="main-area">
                <div id="background-image" class="exhibit-hall-godrej-bg" style="background-image:url(img/hall-dubinor_ointment.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item back-button">
                        <button class="btn btn-back" onclick="history.back()"><i class="fas fa-arrow-alt-circle-left"></i> Back</button>
                    </div>
                    <div class="overlay-item duboint-tv-area">

                    </div>

                    <a class="overlay-item duboint-samples-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subSampleReq"></a>
                    <a class="overlay-item duboint-products-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subProductsReq"></a>
                    <a class="overlay-item duboint-literature-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subLitReq"></a>

                    <div class="overlay-item duboint-banner01-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/dubinor-oint-01.jpg"></a>
                        </div>
                    </div>
                    <div class="overlay-item duboint-banner02-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/dubinor-oint-02.jpg"></a>
                        </div>
                    </div>

                </div>
                <?php require_once "bottom-navmenu.php" ?>



            </div>
        </div>

    </div>

    <?php require_once "commons.php" ?>
    <?php require_once "hallcommons.php" ?>

    <div id="exhibitors-chat"></div>
    <div id="exhib-resources">
        <div class="exhibit-resources text-left">
            <ul class="list-unstyled">
                <?php
                $halls = new Hall();
                $resources = $halls->getExhibResources($exhibitor_id);

                if (!empty($resources)) {
                    foreach ($resources as $resource) {
                ?>
                        <li><i class="far fa-file-pdf"></i> <a href="<?php echo $resource['resource_url']; ?>" data-id="<?php echo $resource['resource_id']; ?>" class="res-dl" target="_blank" download><?php echo $resource['resource_title']; ?></a></li>
                <?php
                    }
                } else {
                    echo 'No resources available.';
                }

                ?>
            </ul>
        </div>
    </div>
    <div id="video-list">
        <div class="exhibitor-videos text-left">
            <ul class="list-unstyled">
                <?php
                $halls = new Hall();
                $videos = $halls->getExhibVideos($exhibitor_id);

                if (!empty($videos)) {
                    foreach ($videos as $video) {
                ?>
                        <li><i class="fas fa-play-circle"></i> <a href="<?php echo $video['video_url']; ?>" data-id="<?php echo $video['video_id']; ?>" data-showsocial="false" class="vid-view html5lightbox"><?php echo $video['video_title']; ?></a></li>
                <?php
                    }
                } else {
                    echo 'No videos available.';
                }

                ?>
            </ul>
        </div>
    </div>
    <div id="contactus">

        <div id="contact-us" class="scroll">
            <div class="contact">
                <h2>Mr. ABC</h2>
                <h4>Director, Godrej Interio </h4>
                <h5>Email: abc@godrej.com</h5>
            </div>
            <div class="contact">
                <h2>Mr. XYZ</h2>
                <h4>Director, Godrej Interio </h4>
                <h5>Email: xyz@godrej.com</h5>
            </div>
            <div class="social">
                <h4>Follow Us:</h4>

                <a href="https://www.facebook.com/godrejinterio" target="_blank"><i class="fab fa-facebook"></i></a>
                <a href="https://twitter.com/GodrejInterio4U" target="_blank"><i class="fab fa-twitter"></i></a>
            </div>

        </div>

    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.popupoverlay.js"></script>
    <script type="text/javascript" src="lightbox/html5lightbox.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script data-loc="<?php echo $exhibitor_id; ?>" src="js/site.js?v=2"></script>
    <script data-hall="<?php echo $exhibitor_id; ?>" src="js/exhibit-hall.js"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>

</body>

</html>