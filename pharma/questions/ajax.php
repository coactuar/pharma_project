<?php
require_once "../controls/config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        case 'getquestions':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            //$sql = "SELECT COUNT(id) FROM tbl_session_questions";  
            $sess = $_POST['session'];
              $sql = "SELECT COUNT(id) FROM tbl_session_questions";  
              if($sess != '1'){
                  $sql .= " where tbl_session_questions.session_id='$sess'";
              }
              //echo $sql;
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <br>
            <div class="row bg-dark p-1">
                <div class="col-6">
                    Total Ques: <div id="ques_count" style="display:inline-block;"><?php echo $total_records; ?></div>
                </div>
                <div class="col-6>"><div id="ques_update"></div></div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="200">Name</th>
                          <th>Question</th>
                          <th width="200">Asked On</th>
                          <th width="300">For the Speaker?</th>
                          <th width="100"></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                      
                      
                        $query="select tbl_session_questions.id, tbl_session_questions.user_id, tbl_session_questions.question,  tbl_session_questions.asked_at, tbl_session_questions.speaker, tbl_session_questions.answered, tbl_users.first_name, tbl_users.last_name, tbl_sessions.session_title from tbl_session_questions, tbl_users, tbl_sessions where tbl_session_questions.user_id=tbl_users.userid and tbl_session_questions.session_id=tbl_sessions.session_id";
                        if($sess != '1'){
                        $query .= " and tbl_session_questions.session_id='$sess'";
                        }
                        
                        $query .= " order by  answered asc, asked_at desc LIMIT 0, $limit";
                        //echo $query;
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        //echo $query;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['first_name'] .' '.$data['last_name'] ; ?></td>
                            <td>
                                <?php echo $data['question']; ?>
                                <!--<br>
                                <?php if($data['reply'] ==''){ ?>
                                <input type="button" data-toggle="modal" data-target="#replyQues" name="reply" value="Reply" id="<?php echo $data['id']; ?>" class="btn btn-sm btn-primary btn-reply">
                                <?php } else { echo '<b>Reply: </b>' . nl2br($data['reply']);?>
                                    <br>
                                    <input type="button" data-toggle="modal" data-target="#updreplyQues" name="reply" value="Update" id="<?php echo $data['id']; ?>" class="btn btn-sm btn-primary btn-update">  
                                <?php } ?>-->
                            </td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            <td>
                            <?php if ($data['answered'] == '0') { ?>
                            <a href="#" class="btnSpk btn btn-sm <?php if ($data['speaker'] == '0') { echo 'btn-danger'; } else { echo 'btn-success'; } ?>" onClick="updSpk('<?php echo $data['id']; ?>','<?php echo $data['speaker']; ?>')"><?php if ($data['speaker'] == '0') { echo 'Yes/No?'; } else { echo 'Cancel?'; } ?></a>
                            <?php 
                            
                            if ($data['speaker'] == '1') { ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Answered</a>
                            <?php } 
                            }
                            else
                            {
                             ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Unanswered</a>
                            <?php    
                            }
                            ?>
                            </td>
                            <td>
                            <a href="#" class="btn btn-sm btn-danger" onClick="delQues('<?php echo $data['id']; ?>')">Delete</a>
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getques':
              $sql = "SELECT question FROM tbl_session_questions where id='".$_POST['quesid']."'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $ques = $row[0];  
              
              echo $ques;
        break;
        
        case 'getreply':
              $sql = "SELECT reply FROM tbl_session_questions where id='".$_POST['quesid']."'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $reply = $row[0];  
              
              echo $reply;
        break;
        
        case 'getquesupdate':
                $sess = $_POST['session'];
              $sql = "SELECT COUNT(id) FROM tbl_session_questions";  
              if($sess != '1'){
                  $sql .= " where tbl_session_questions.session_id='$sess'";
              }
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $total_records = $row[0];  
              
              echo $total_records;
        break;
        
        case 'updateques':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_session_questions set show_speaker ='$newval' where id = '".$_GET['id']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'updatespk':
              $newval = !$_POST['val'];
              /*if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }*/
              $sql = "Update tbl_session_questions set speaker ='$newval', answered='0' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              echo $sql;
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
         case 'updatespkans':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_session_questions set answered ='$newval' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'delques':
        
            $sql = "delete from tbl_session_questions where id = '".$_POST['ques']."'";  
            $rs_result = mysqli_query($link,$sql); 
        
        break;
        
        
        case 'getpollresults':

          $poll_id = $_POST['pid'];
    
          $query = 'select count(*) as count from tbl_pollanswers where poll_id = "$poll_id"';
          $rs = mysqli_query($link, $query);
          
          $data=mysqli_fetch_assoc($rs);
    
          $pollAnsCount = $data['count'];
          //            echo $pollAnsCount;
          $total_count = $pollAnsCount;
    
          $query = "select * from tbl_polls where poll_id = '$poll_id' limit 1";
          $rs = mysqli_query($link, $query);
          
          $data=mysqli_fetch_assoc($rs);
          
          //var_dump( $curr_poll);
          $poll_ques = $data['poll_question'];
          $poll_opt1 = $data['poll_opt1'];
          $poll_opt2 = $data['poll_opt2'];
          $poll_opt3 = $data['poll_opt3'];
          $poll_opt4 = $data['poll_opt4'];
          $poll_opt5 = $data['poll_opt5'];
          $corrans = $data['correct_ans'];
    
          echo "<h6><b>Question: </b>" . $poll_ques . "</h6>";
    
          $q = 'select count(*) as count from tbl_pollanswers where poll_id = "$poll_id" and poll_answer ="opt1"';
          $r = mysqli_query($link, $q);
          
          $d=mysqli_fetch_assoc($r);
          
          $opt1_count = $d['count'];
          $opt1width = 0;
          if ($total_count != '0') {
            $opt1width = ($opt1_count / $total_count) * 100;
          }
          $ans = '';
          if ($corrans == 'opt1') {
            $ans = 'corrans';
          }
        ?>
          <div class="option"> <strong><?php echo $poll_opt1; ?></strong><span class="float-right badge badge-success"><?php echo $opt1width . '%'; ?></span>
            <div class="progress <?php echo $ans; ?>">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: <?php echo $opt1width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </div>
          <?php
          $q = 'select count(*) as count from tbl_pollanswers where poll_id = "$poll_id" and poll_answer ="opt2"';
          $r = mysqli_query($link, $q);
          
          $d=mysqli_fetch_assoc($r);
          
          $opt2_count = $d['count'];
          $opt2width = 0;
          if ($total_count != '0') {
            $opt2width = ($opt2_count / $total_count) * 100;
          }
          $ans = '';
          if ($corrans == 'opt2') {
            $ans = 'corrans';
          }
          ?>
          <div class="option"> <strong><?php echo $poll_opt2; ?></strong><span class="float-right  badge badge-primary"><?php echo $opt2width . '%'; ?></span>
            <div class="progress <?php echo $ans; ?>">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: <?php echo $opt2width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </div>
          <?php
          if ($poll_opt3 != '') {
            $q = 'select count(*) as count from tbl_pollanswers where poll_id = "$poll_id" and poll_answer ="opt3"';
          $r = mysqli_query($link, $q);
          
          $d=mysqli_fetch_assoc($r);
          
          $opt3_count = $d['count'];
            $opt3width = 0;
            if ($total_count != '0') {
              $opt3width = ($opt3_count / $total_count) * 100;
            }
            $ans = '';
            if ($corrans == 'opt3') {
              $ans = 'corrans';
            }
          ?>
            <div class="option"> <strong><?php echo $poll_opt3; ?></strong><span class="float-right badge badge-warning"><?php echo $opt3width . '%'; ?></span>
              <div class="progress <?php echo $ans; ?>">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: <?php echo $opt3width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
          <?php
          }
          if ($poll_opt4 != '') {
            $q = 'select count(*) as count from tbl_pollanswers where poll_id = "$poll_id" and poll_answer ="opt4"';
          $r = mysqli_query($link, $q);
          
          $d=mysqli_fetch_assoc($r);
          
          $opt4_count = $d['count'];
          $opt4width = 0;
            if ($total_count != '0') {
              $opt4width = ($opt4_count / $total_count) * 100;
            }
            $ans = '';
            if ($corrans == 'opt4') {
              $ans = 'corrans';
            }
          ?>
            <div class="option"> <strong><?php echo $poll_opt4; ?></strong><span class="float-right badge badge-danger"><?php echo $opt4width . '%'; ?></span>
              <div class="progress <?php echo $ans; ?>">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: <?php echo $opt4width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
    <?php
          }
          
          if ($poll_opt5 != '') {
            $q = 'select count(*) as count from tbl_pollanswers where poll_id = "$poll_id" and poll_answer ="opt5"';
          $r = mysqli_query($link, $q);
          
          $d=mysqli_fetch_assoc($r);
          
          $opt5_count = $d['count'];
            $opt5width = 0;
            if ($total_count != '0') {
              $opt5width = ($opt5_count / $total_count) * 100;
            }
            $ans = '';
            if ($corrans == 'opt5') {
              $ans = 'corrans';
            }
          ?>
            <div class="option"> <strong><?php echo $poll_opt4; ?></strong><span class="float-right badge badge-info"><?php echo $opt5width . '%'; ?></span>
              <div class="progress <?php echo $ans; ?>">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: <?php echo $opt5width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
    <?php
          }

      break;
        



        
    }
    
}


?>