<?php
	require_once "../controls/config.php";
if(!isset($_GET['s'])){
    
        header('location: ./');
    
}
    
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Polls</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="questions.php?s=<?php echo $_GET['s'] ?>">Questions</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="polls.php?s=<?php echo $_GET['s'] ?>">Polls</a>
      </li>
      
    </ul>
    
    
  </div>
</nav>
<div class="container-fluid">
     <div class="row mt-1">
        <div class="col-12">
            <?php
                $sess = $_GET['s'];
                
                            
                $sql = "select * from tbl_polls where session_id='$sess'";
                $rs = mysqli_query($link, $sql);
                if(mysqli_affected_rows($link) > 0){
                  ?>
                  <table class="table table-striped table-dark"> 
                  <?php
                  while($data = mysqli_fetch_assoc($rs))
                  {
                   ?>
                   <tr>
                    <td><?php echo $data['poll_question']; ?></td>
                    <td width="200"><a class="btn btn-success" href="pollresults.php?s=<?php echo $_GET['s'] ?>&id=<?php echo $data['poll_id'] ?>">View Results</a></td>
                   </tr>
                   <?php
                  }
                  ?>
                   </table>
                   <?php
                }
                else{
                    echo 'No Polls in this session.';
                }
                
            ?>
        </div>
    </div>
     
    
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>