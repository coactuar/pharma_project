<?php
	require_once "../controls/config.php";
    
    $poll_id = 0;
    if(isset($_GET['id'])){
        $poll_id = $_GET['id'];
        
        $sql = "select * from tbl_polls where poll_id='$poll_id'";
        $rs = mysqli_query($link, $sql);
        
        if(mysqli_affected_rows($link) < 1)
        {
            header('location: polls.php?s='.$_GET['s']);
        }

    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Poll Results</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="questions.php?s=<?php echo $_GET['s'] ?>">Questions</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="polls.php?s=<?php echo $_GET['s'] ?>">Polls</a>
      </li>
      
    </ul>
    
    
  </div>
</nav>

<div class="container-fluid bg-white color-grey">
     
    <?php
    //$curr_poll = $poll->getPoll($poll_id);
//    var_dump($curr_poll);
    ?>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <div id="message"></div>
            <div id="pollresults"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getPollResult();
});


function getPollResult()
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollresults', pid: '<?php echo $poll_id; ?>'},
        type: 'post',
        success: function(response) {
            $("#pollresults").html(response);
            setTimeout(function(){ getPollResult(); }, 5000);
            
        }
    });
}


</script>

</body>
</html>