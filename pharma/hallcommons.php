<div id="req-samples">
    <div id="req-sample-msg">
        Hello, <?php echo $user_name; ?>!
        <br><br>
        <div class="succ-msg"> THANKS. <br>WE HAVE REGISTERED YOUR REQUEST FOR SAMPLES!</div>
    </div>
</div>
<div id="req-products">
    <div id="req-prod-msg">
        Hello, <?php echo $user_name; ?>!
        <br><br>
        <div class="succ-msg"> THANKS. <br>WE HAVE REGISTERED YOUR REQUEST FOR PRODUCT DETAILING!</div>
    </div>
</div>
<div id="req-literature">
    <div id="req-lit-msg">
        Hello, <?php echo $user_name; ?>!
        <br><br>
        <div class="succ-msg"> THANKS. <br>WE HAVE REGISTERED YOUR REQUEST FOR LITERATURE!</div>
    </div>
</div>