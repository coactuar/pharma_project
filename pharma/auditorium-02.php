<?php
require_once "controls/sesCheck.php";
require_once "functions.php";
$curr_room = 'auditorium02';
$audi_name = 'Auditorium 02';
$audi = new Session();
$audi02 = $audi->getAudi($audi_name);
$audi_id = $audi02[0]['audi_id'];

$session_id = 0;
if (isset($_GET['ses'])) {
    $session_id = $_GET['ses'];
    $valid = $audi->isValidSession($session_id);

    if (!$valid) {
        header('location: auditorium-02');
    }
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Auditorium 02:: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>

</head>

<body>


    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <div class="navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>
        <div class="page-wrapper">

            <div id="main-area">
                <div id="background-image" class="auditorium-bg" style="background-image:url(img/auditorium02_area.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item audi-video" href="#">
                        <?php
                        $session = new Session();
                        $webcastUrl = $session->getWebcastSessionURL($session_id);
                        ?>
                        <div id="player"></div>
                    </div>
                    <div class="overlay-item audi-agenda">
                        <i class="fa fa-table"></i>
                        <a href="#" id="show_sessions">Agenda</a>
                    </div>
                    <?php if (isset($_GET['ses'])) { ?>
                        <div class="overlay-item audi-ques">
                            <i class="fas fa-question-circle"></i>
                            <a href="#" id="audi_askques">Ask Question</a>
                        </div>
                        <div class="overlay-item audi-poll">
                            <i class="fas fa-poll"></i>
                            <a href="#" id="audi_takepoll">Take Polls</a>
                        </div>
                        <div class="overlay-item panel ques">
                            <div class="panel-heading">
                                Ask A Question
                                <a href="#" class="close" id="close_ques"><i class="fas fa-times"></i></a>
                            </div>
                            <div class="panel-content">
                                <div id="ques-message" style="display:none;"></div>
                                <form>
                                    <div class="form-group">
                                        <textarea class="input" rows="6" name="sesques" id="sesques"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" name="send_sesques" data-ses="<?php echo $session_id; ?>" data-user="<?php echo $_SESSION['user_id']; ?>" class="send_sesques btn-sendmsg">Submit Question</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="overlay-item panel poll">
                            <div class="panel-heading">
                                Take Poll
                                <a href="#" class="close" id="close_poll"><i class="fas fa-times"></i></a>
                            </div>
                            <div class="panel-content">
                                <div id="poll-message" style="display:none;"></div>
                                <div id="currpollid" style="display:none;">0</div>
                                <div id="currpoll" style="display:none;">

                                </div>
                                <div id="currpollresults" style="display:none">

                                </div>
                            </div>

                        </div>

                    <?php } ?>
                    <div class="overlay-item audi-disclaimer">
                        Integrace Pvt Ltd has obtained the necessary consent from the respective copyright holders for relaying, sharing & archiving the materials appearing in this program. However, Integrace Pvt Ltd accepts no role or liability for the medical opinion, accuracy or completeness of the information contained in this publication or its update.
                    </div>
                </div>
                <?php require_once "bottom-navmenu.php" ?>


            </div>
        </div>

    </div>

    <?php require_once "commons.php" ?>
    <div id="webcast_sessions" class="scroll popup-dialog">

        <div class="tabs">
            <a href="#" id="tab-day1" onClick="showDay1('<?php echo $audi_id; ?>')" class="active">Day 1 Saturday, 8th August 2020</a>
            <a href="#" id="tab-day2" onClick="showDay2('<?php echo $audi_id; ?>')" class="">Day 2 Sunday, 9th August 2020</a>
        </div>
        <div id="sessions-day1" style="display:block;">
            <div id="day1" class="sessions scroll">Day 1</div>
        </div>
        <div id="sessions-day2" style="display:none;">
            <div id="day2" class="sessions scroll">Day 2</div>
        </div>
        <!-- <div id="sessions"></div> -->
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.popupoverlay.js"></script>

    <script type="text/javascript" src="lightbox/html5lightbox.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script data-loc="<?php echo $curr_room; ?>" src="js/site.js?v=2"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>
    <script src="js/webcast.js"></script>
    <?php
    if (isset($_GET['ses'])) {
    ?>
        <script>
            var player = new Clappr.Player({
                source: "<?php echo $webcastUrl; ?>",
                parentId: "#player",
                width: "100%",
                height: "100%",

                autoplay: false,
                poster: "img/boot-poster.jpg",
                plugins: [LevelSelector],
                levelSelectorConfig: {
                    title: 'Quality',
                    labels: {
                        3: '1080p',
                        2: '720p',
                        1: '480p',
                        0: '360p',
                    },
                    labelCallback: function(playbackLevel, customLabel) {
                        return customLabel;
                    }
                },

            });

            player.play();
        </script>
    <?php } ?>
    <script>
        $('#webcast_sessions').dialog({
            autoOpen: false,
            title: 'Sessions Agenda - <?php echo $audi_name; ?>',
            resizable: false,
            position: {
                my: "center center",
                at: "center center"
            }
        });


        $(document).on('click', '#show_sessions', function() {
            getSessions('<?php echo $audi_id; ?>', 'day1', '');
            getSessions('<?php echo $audi_id; ?>', 'day2', '');
            $('#webcast_sessions').dialog('open');
        });
        $(document).on('click', '#refresh-sessions', function() {
            var keyword = $('#session-search').val();
            getSessions(keyword);
        });
        $(document).on('click', '#search-session', function() {
            var keyword = $('#session-search').val();
            getSessions(keyword);
            return false;
        });
        $(document).on('click', '#clear-search-session', function() {
            $('#session-search').val('');
            getSessions('');
            return false;
        });



        function updateSession(web) {
            $.ajax({
                url: 'controls/server.php',
                data: {
                    action: 'updatesession',
                    sessId: web
                },
                type: 'post',
                success: function(output) {
                    //getAttendeesCount(web);
                    updSes = setTimeout(function() {
                        updateSession(web);
                    }, 10000);
                }
            });
        }

        <?php
        if (!isset($_GET['ses'])) {
        ?>
            //getSessions('<?php echo $audi_id; ?>', 'day1', '');
            //getSessions('<?php echo $audi_id; ?>', 'day2', '');
            //$('#webcast_sessions').dialog('open');

            function checkforLive(curr, audi) {
                $.ajax({
                    url: 'controls/server.php',
                    data: {
                        action: 'checkforlive',
                        sessId: curr,
                        audiId: audi
                    },
                    type: 'post',
                    success: function(output) {
                        //getAttendeesCount(web);
                        console.log(output);

                        if (output == '-1') {
                            console.log('lets go home');
                            url = 'auditorium-02.php';
                            location.href = url;
                            console.log(url);
                        } else
                        if (output == '0') {
                            console.log('lets stay here');
                        } else {
                            console.log('lets go live');
                            url = 'auditorium-02.php?ses=' + output;
                            location.href = url;
                            console.log(url);
                        }
                        setTimeout(function() {
                            checkforLive(curr, audi);
                        }, 15000);
                    }
                });
            }
            checkforLive('<?php echo $session_id; ?>', '<?php echo $audi_id ?>');
        <?php } else { ?>

            function chknewpoll(session) {
                $.ajax({
                    url: 'controls/server.php',
                    data: {
                        action: 'checknewpoll',
                        sessId: session
                    },
                    type: 'post',
                    success: function(output) {
                        var cpoll = $('#currpollid').text();
                        console.log(cpoll);
                        console.log(output);
                        if (cpoll == output) {
                            console.log('dont change');
                        } else {
                            console.log('update poll');
                            updatePoll(output, session);

                        }
                        $('#currpollid').text(output);


                    }
                });


                chkPoll = setTimeout(function() {
                    chknewpoll('<?php echo $session_id; ?>');
                }, 10000);

                chkPollRes = setTimeout(function() {
                    chkpollres('<?php echo $session_id; ?>');
                }, 10000);

            }

            function updatePoll(pollid, session) {
                $.ajax({
                    url: 'controls/server.php',
                    data: {
                        action: 'updatepoll',
                        pollId: pollid,
                        sessId: session
                    },
                    type: 'post',
                    success: function(output) {

                        $('#currpoll').html(output);
                        $('#currpoll').css('display', 'block');

                    }
                });
            }

            function chkpollres(session) {
                $.ajax({
                    url: 'controls/server.php',
                    data: {
                        action: 'checkpollres',
                        sessId: session
                    },
                    type: 'post',
                    success: function(output) {
                        //getAttendeesCount(web);
                        console.log('PR: ' + output);
                        var cpoll = $('#currpollresults').text();

                        if (cpoll == output) {
                            //
                        } else {
                            //clearTimeout(chkPoll);
                            $('#currpollresults').html(output);
                            $('#currpollresults').css('display', 'block');
                        }


                    }
                });



            }

            updateSession('<?php echo $session_id; ?>');
            chknewpoll('<?php echo $session_id; ?>');
            //chkpollres('<?php echo $session_id; ?>');
        <?php } ?>
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-93480057-20');
    </script>

</body>

</html>