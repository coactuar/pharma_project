<?php
require_once "controls/sesCheck.php";
require_once "functions.php";

$exhibit_hall_id = 'bon_dk';
$exhibitor_id = '86165436c48a17e2ef80e2557db03175f2c7bd9fd27fb8b4';

$halls = new Hall();
$verify = $halls->verifyHallId($exhibitor_id);
if (empty($verify)) {
    header('location: exhibition-halls');
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Bon DK :: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

</head>

<body>
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <div class="navbar-collapse">

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>

        <div class="page-wrapper">
            <div id="main-area">
                <div id="background-image" class="exhibit-hall-godrej-bg" style="background-image:url(img/hall-bon_dk.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item back-button">
                        <button class="btn btn-back" onclick="history.back()"><i class="fas fa-arrow-alt-circle-left"></i> Back</button>
                    </div>
                    <div class="overlay-item bondk-tv-area">
                        <div class="exhib-actions">
                            <a class="hall-video vid-view" data-vidid="ddbd2cf7942eb410eb0c14269f8e9ceca00743e1491d693f" href="https://coact.live/pharma/video-bondk.php"></a>
                        </div>
                    </div>

                    <a class="overlay-item bondk-samples-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subSampleReq"></a>
                    <a class="overlay-item bondk-products-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subProductsReq"></a>
                    <a class="overlay-item bondk-literature-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subLitReq"></a>

                    <div class="overlay-item bondk-banner01-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/bondk-01.jpg"></a>
                        </div>
                    </div>
                    <div class="overlay-item bondk-banner02-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/bondk-02.jpg"></a>
                        </div>
                    </div>

                </div>
                <?php require_once "bottom-navmenu.php" ?>





            </div>
        </div>

    </div>

    <?php require_once "commons.php" ?>
    <?php require_once "hallcommons.php" ?>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.popupoverlay.js"></script>
    <script type="text/javascript" src="lightbox/html5lightbox.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script data-loc="<?php echo $exhibitor_id; ?>" src="js/site.js?v=2"></script>
    <script data-hall="<?php echo $exhibitor_id; ?>" src="js/exhibit-hall.js"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>

</body>

</html>