<?php
spl_autoload_register(function($classname){
    $path = 'model/' . strtolower($classname).".php";
    //echo $path.'<br>'; 
    if(file_exists($path)){
        require_once($path);
        //echo "File $path is found.<br>";
    }
    else{
        echo "File $path is not found.";
    }
});

function tosecs($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}
