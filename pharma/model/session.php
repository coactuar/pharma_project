<?php
//namespace Genseer;

//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once 'config.php';
//require_once 'config.php';
require_once 'constants.php';

class Session
{

    private $ds;

    function __construct()
    {
        //require_once "../lib/datasource.php";
        //require_once __ROOT__ . "/lib/config.php";
        $this->ds = new DataSource();
    }

    public function getAudiList()
    {
        $query = "select * from tbl_auditoriums where active='1' order by audi_name";
        $paramType = '';
        $paramValue = array();

        $audis = $this->ds->select($query, $paramType, $paramValue);

        return $audis;
    }

    public function addSession()
    {
        $errors = [];
        $succ = '';

        $audi = '0';
        $title = '';
        $short = '';
        $long = '';
        $start = 'day1';
        $da = '';
        $url = '';


        $audi = $_POST['audi'];
        $title = $_POST['title'];
        if (isset($_POST['shortdes'])) {
            $short = $_POST['shortdes'];
        }
        if (isset($_POST['longdes'])) {
            $long = $_POST['longdes'];
        }
        $start = $_POST['start_time'];
        $day = $_POST['day'];
        if (isset($_POST['webcasturl'])) {
            $url = $_POST['webcasturl'];
        }

        $sessionid = bin2hex(random_bytes(24)); // generate unique token
        $launch_status = 0;
        $session_status = 'yet';
        $show_session = 1;

        $query = "Insert into tbl_sessions(session_id,audi_id, session_title, session_short, session_long, start_time, day, launch_status, session_status,session_webcast_url, show_session) values(?,?,?,?,?,?,?,?,?,?,?)";
        $paramType = 'sssssssissi';
        $paramValue = array(
            $sessionid,
            $audi,
            $title,
            $short,
            $long,
            $start,
            $day,
            $launch_status,
            $session_status,
            $url,
            $show_session
        );
        $sesid = $this->ds->insert($query, $paramType, $paramValue);

        return $sesid;
    }

    public function editSession($sessionid)
    {
        $errors = [];
        $succ = '';

        $audi = '0';
        $title = '';
        $short = '';
        $long = '';
        $start = '';
        $day = '';
        $url = '';


        $audi = $_POST['audi'];
        $title = $_POST['title'];
        if (isset($_POST['shortdes'])) {
            $short = $_POST['shortdes'];
        }
        if (isset($_POST['longdes'])) {
            $long = $_POST['longdes'];
        }
        $start = $_POST['start_time'];

        if (isset($_POST['webcasturl'])) {
            $url = $_POST['webcasturl'];
        }
        $day = $_POST['day'];


        $query = "Update tbl_sessions set audi_id=?, session_title=?, session_short=?, session_long=?, start_time=?, day=?,session_webcast_url=? where session_id=?";
        $paramType = 'ssssssss';
        $paramValue = array(
            $audi,
            $title,
            $short,
            $long,
            $start,
            $day,
            $url,
            $sessionid,
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function isValidSession($sessionid)
    {
        $query = "select * from tbl_sessions where session_id = ? and launch_status='1' limit 1";
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $status;
    }
    public function isSession($sessionid)
    {
        $query = "select * from tbl_sessions where session_id = ? limit 1";
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $status;
    }

    public function getSessionList($keyword = '')
    {
        $query = "select * from tbl_sessions where show_session='1' and (session_title like '%$keyword%' or  session_short like '%$keyword%' or  session_long like '%$keyword%') order by start_time";
        $paramType = '';
        $paramValue = array();

        $sessions = $this->ds->select($query, $paramType, $paramValue);

        return $sessions;
    }

    public function getSession($sessionid)
    {
        $query = "select * from tbl_sessions where session_id=? limit 1";
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $wcsessions = $this->ds->select($query, $paramType, $paramValue);

        return $wcsessions;
    }

    public function getWebcastSessionTitle($webcastid)
    {
        $query = "select session_title from tbl_sessions where session_id=? limit 1";
        $paramType = 's';
        $paramValue = array(
            $webcastid
        );

        $wcsessions = $this->ds->select($query, $paramType, $paramValue);

        return $wcsessions[0]['session_title'];
    }

    public function getWebcastSessionURL($webcastid)
    {
        $query = "select session_webcast_url from tbl_sessions where session_id=? limit 1";
        $paramType = 's';
        $paramValue = array(
            $webcastid
        );

        $wcsessions = $this->ds->select($query, $paramType, $paramValue);

        return $wcsessions[0]['session_webcast_url'];
    }

    public function updateMemberSessionStatus($webcastid, $userid)
    {
        $today = date('Y/m/d H:i:s', time());

        $query = "select * from tbl_sessions_attendee where session_id = ? and user_id=? and leave_time >= ? limit 1";
        $paramType = 'sss';
        $paramValue = array(
            $webcastid,
            $userid,
            $today
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);
        if ($status > 0) {
            $leave_time  = date('Y/m/d H:i:s', time() + 30);
            $query = "UPDATE tbl_sessions_attendee set leave_time=? where user_id=? and session_id=? and leave_time >= ?";
            $paramType = 'ssss';
            $paramValue = array(
                $leave_time,
                $userid,
                $webcastid,
                $today
            );
            $this->ds->execute($query, $paramType, $paramValue);
        } else {
            $join_time  = date('Y/m/d H:i:s', time());
            $leave_time  = date('Y/m/d H:i:s', time() + 30);
            $query = "Insert into tbl_sessions_attendee(session_id, user_id, join_time, leave_time) values(?,?,?,?)";
            $paramType = 'ssss';
            $paramValue = array(
                $webcastid,
                $userid,
                $join_time,
                $leave_time
            );
            $sesid = $this->ds->insert($query, $paramType, $paramValue);
        }
    }

    public function getSessionAttendeesCount($webcastid)
    {
        $today = date('Y/m/d H:i:s', time());

        $query = "select * from tbl_sessions_attendee where session_id=? and leave_time >= ?";
        $paramType = 'ss';
        $paramValue = array(
            $webcastid,
            $today
        );

        $attendees = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $attendees;
    } //

    public function getSessionAttendees($webcastid)
    {
        $today = date('Y/m/d H:i:s', time());

        $query = "select first_name, last_name from tbl_sessions_attendee, tbl_users where tbl_sessions_attendee.user_id = tbl_users.id and session_id=? and leave_time >= ?";
        $paramType = 'ss';
        $paramValue = array(
            $webcastid,
            $today
        );

        $attendees = $this->ds->select($query, $paramType, $paramValue);
        return $attendees;
    } //

    public function getSessionMessages($webcastid)
    {
        $today = date('Y/m/d H:i:s', time());

        $query = "select first_name, last_name, chat_time, message from tbl_session_chat, tbl_users where tbl_session_chat.user_id = tbl_users.id and tbl_session_chat.session_id = ? order by chat_time desc";
        $paramType = 's';
        $paramValue = array(
            $webcastid,
        );

        $messages = $this->ds->select($query, $paramType, $paramValue);
        return $messages;
    } //

    public function sendMessage($session_id, $user_id, $message)
    {
        $today = date("Y/m/d H:i:s");

        $query = "insert into tbl_session_chat(session_id, user_id, message, chat_time) values (?,?,?,?) ";
        $paramType = 'ssss';
        $paramValue = array(
            $session_id,
            $user_id,
            $message,
            $today
        );
        $msgid = $this->ds->insert($query, $paramType, $paramValue);
        return $msgid;
    }


    public function updateEntryStatus($sessionid, $val)
    {
        $query = "Update tbl_sessions set launch_status =? where session_id = ?";
        $paramType = 'ss';
        $paramValue = array(
            $val,
            $sessionid
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function updateSessionStatus($sessionid, $val)
    {
        $query = "Update tbl_sessions set session_status =? where session_id = ?";
        $paramType = 'ss';
        $paramValue = array(
            $val,
            $sessionid
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function updateShowStatus($sessionid, $val)
    {
        $query = "Update tbl_sessions set show_session =? where session_id = ?";
        $paramType = 'ss';
        $paramValue = array(
            $val,
            $sessionid
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function delSession($sessionid)
    {
        $query = "delete from tbl_sessions where session_id=?";
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function getAudi($audiname)
    {

        $query = "select * from tbl_auditoriums where active='1' and audi_name =?";
        $paramType = 's';
        $paramValue = array(
            $audiname
        );

        $audis = $this->ds->select($query, $paramType, $paramValue);

        return $audis;
    }

    public function getAudiSessionList($audiid, $day, $keyword)
    {
        $query = "select * from tbl_sessions where audi_id=? and day=? and show_session = '1'";
        $query .= " and (session_title like '%$keyword%'";
        $query .= " or  session_short like '%$keyword%'";
        $query .= " or  session_long like '%$keyword%')";
        $query .= "  order by start_time asc";

        $paramType = 'ss';
        $paramValue = array(
            $audiid,
            $day
        );

        $audiSess = $this->ds->select($query, $paramType, $paramValue);

        return $audiSess;
    }

    public function getCurrLiveSession($audi)
    {
        $query = "select * from tbl_sessions where audi_id=? and live_status = '1' and launch_status='1'";

        $paramType = 's';
        $paramValue = array(
            $audi
        );

        $audiSess = $this->ds->select($query, $paramType, $paramValue);

        return $audiSess;
    }

    public function getLiveSessionViewerCount($sessId)
    {
        $today = date("Y/m/d H:i:s", time() + 15);

        $query = "select * from tbl_sessions_attendee where session_id=? and leave_time > ?";

        $paramType = 'ss';
        $paramValue = array(
            $sessId,
            $today
        );

        $audiSess = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $audiSess;
    }

    public function getLiveSessionViewers($sessId)
    {
        $today = date("Y/m/d H:i:s", time() + 15);

        $query = "select * from tbl_sessions_attendee, tbl_users where tbl_sessions_attendee.user_id=tbl_users.userid and session_id=? and leave_time > ?";

        $paramType = 'ss';
        $paramValue = array(
            $sessId,
            $today
        );

        $audiSess = $this->ds->select($query, $paramType, $paramValue);

        return $audiSess;
    }

    public function getSessionQuestions($sessId)
    {
        //        $today = date("Y/m/d H:i:s");

        $query = "select * from tbl_session_questions, tbl_users where tbl_session_questions.user_id=tbl_users.userid and session_id=? order by asked_at desc";

        $paramType = 's';
        $paramValue = array(
            $sessId
        );

        $ques = $this->ds->select($query, $paramType, $paramValue);

        return $ques;
    }

    public function askQuestion($session_id, $user_id, $question)
    {
        $today = date("Y/m/d H:i:s");

        $query = "insert into tbl_session_questions(session_id, user_id, question, asked_at) values (?,?,?,?) ";
        $paramType = 'ssss';
        $paramValue = array(
            $session_id,
            $user_id,
            $question,
            $today
        );
        $msgid = $this->ds->insert($query, $paramType, $paramValue);
        return $msgid;
    }
}
