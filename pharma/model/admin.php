<?php
//require_once 'config.php';
require_once 'constants.php';
class Admin
{

    private $ds;

    function __construct()
    {
        $this->ds = new DataSource();
    }

    public function getTeamChatUsers()
    {
        $query = 'SELECT distinct(user_id_from),max(chat_time) as chat_time FROM tbl_team_chat, tbl_users where tbl_team_chat.user_id_from=tbl_users.userid group by user_id_from order by max(chat_time) DESC';
        $paramType = '';
        $paramValue = array();
        $users = $this->ds->select($query, $paramType, $paramValue);

        return $users;
    }

    public function getUnreadChatCount($userid)
    {
        $query = "select * from tbl_team_chat where read_status ='0' and user_id_from=? and user_id_to='team'";
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getTeamChatHistory($user_from, $user_to)
    {
        $query = "select first_name, last_name, message, source, user_id_from, user_id_to, chat_time from tbl_team_chat,tbl_users where ((user_id_from=? AND user_id_to =?) OR (user_id_from=? AND user_id_to=?)) AND (tbl_team_chat.user_id_from = tbl_users.userid OR tbl_team_chat.user_id_to = tbl_users.userid) order by chat_time asc";
        $paramType = 'ssss';
        $paramValue = array(
            $user_from,
            $user_to,
            $user_to,
            $user_from
        );
        $history = $this->ds->select($query, $paramType, $paramValue);

        return $history;
    }

    public function updateTeamReadStatus($user_from, $user_to)
    {
        $query = "update tbl_team_chat set read_status = '1' where user_id_to =? and user_id_from =?";
        $paramType = 'ss';
        $paramValue = array(
            $user_to,
            $user_from
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function sendTeamMsg($to, $from, $msg, $src)
    {
        $chat_time   = date('Y/m/d H:i:s');
        $query = "insert into tbl_team_chat(user_id_from, user_id_to, message, chat_time, source) values(?, ?, ?, ?, ?)";
        $paramType = 'sssss';
        $paramValue = array(
            $from,
            $to,
            $msg,
            $chat_time,
            $src
        );

        $msgid = $this->ds->insert($query, $paramType, $paramValue);
        return $msgid;
    }


    /* Dashboard Functions */
    public function getMemberCount()
    {
        $query = "SELECT * FROM tbl_users";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getVerMemberCount()
    {
        $query = "SELECT * FROM tbl_users where verified='1'";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }
    public function getVisitorCount()
    {
        $query = "SELECT DISTINCt user_id FROM tbl_user_logins";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getHallsCount()
    {
        $query = "SELECT * FROM tbl_exhibitors";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getActHallsCount()
    {
        $query = "SELECT * FROM tbl_exhibitors where active='1'";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getResCount()
    {
        $query = "select * from tbl_exhibitor_resources";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getResDLCount()
    {
        $query = "select sum(download_count) as total from tbl_exhibitor_resources";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    public function getResDLList($count)
    {
        $query = "select exhibitor_name, resource_title, download_count from tbl_exhibitor_resources, tbl_exhibitors where download_count != '0' and tbl_exhibitor_resources.exhibitor_id=tbl_exhibitors.exhibitor_id order by download_count desc limit ?";
        $paramType = 'i';
        $paramValue = array(
            $count
        );

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    public function getVidCount()
    {
        $query = "select * from tbl_exhibitor_videos";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getVidViewsCount()
    {
        $query = "select * from tbl_exhibitor_videos_views";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }
    public function getVidViewsList($count)
    {
        $query = "SELECT video_id, count(video_id) as count FROM `tbl_exhibitor_videos_views` GROUP by video_id ORDER BY count DESC limit ?";
        $paramType = 'i';
        $paramValue = array(
            $count
        );

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    public function getSessionsCount()
    {
        $query = "select * from tbl_sessions";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getSessionAttendeeCount()
    {
        $query = "SELECT count(DISTINCT user_id) as cnt, tbl_sessions_attendee.session_id FROM `tbl_sessions_attendee`, tbl_sessions where tbl_sessions_attendee.session_id=tbl_sessions.session_id group by tbl_sessions_attendee.session_id order by cnt desc limit 10";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    public function getSessionAttended()
    {
        $query = "select count(distinct user_id) as cnt from tbl_sessions_attendee";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count[0]['cnt'];
    }
    public function getSessionAttendees()
    {
        $query = "SELECT count(distinct session_id) as cnt, user_id FROM `tbl_sessions_attendee`, tbl_users where tbl_sessions_attendee.user_id=tbl_users.userid GROUP by user_id order by cnt desc limit 10";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }
    
    public function getTotalTimeSpent()
    {
        $query = "SELECT SUM(TIMESTAMPDIFF(SECOND, join_time, leave_time)) as total FROM `tbl_user_logins`";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count[0]['total'];
    }

}
