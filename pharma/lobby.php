<?php
require_once "controls/sesCheck.php";
$curr_room = 'lobby';
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Lobby :: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
</head>

<body>
    <div id="main-wrapper">
        <header class="topbar color-grey">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-collapse">
                    <ul class="navbar-nav ml-auto top-right-menu">
                        <!--<li class="nav-item">
                            <div class="live-msg btn btn-danger btn-sm">Event is LIVE in Auditorium 1,2,3</div>
                        </li>-->
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div class="page-wrapper">
            <div id="main-area" style="display:block;">
                <div id="background-image" class="lobby-bg" style="background-image:url(img/lobby_area.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item lobby-video-area">
                        <div class="exhib-actions">
                            <a class="hall-video" href="https://coact.live/pharma/video-lobby.php"></a>
                        </div>
                    </div>

                    <a class="overlay-item lobby-auditorium-01" href="auditorium-01.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-auditorium-02" href="auditorium-02.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-auditorium-03" href="auditorium-03.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-exhibit-hall" href="exhibition-halls.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-agenda agenda_open" href="#">
                        <div class="indicator d-6"></div>
                    </a>
                    <a class="overlay-item lobby-resources resources_open" href="#">
                        <div class="indicator d-6"></div>
                    </a>

                </div>

                <?php require_once "bottom-navmenu.php" ?>

            </div>

        </div>

    </div>

    <?php require_once "commons.php" ?>

    <div id="resources" class="scroll popup-dialog">
        <a class="resources_close popup-close" href="#"><i class="fas fa-times"></i></a>
        <div class="heading">
            <h4>Resources</h4>
        </div>
        <div class="popup-content">
            <ul class="list-unstyled">
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_OTA_President.pdf">OTA President Profile</a></li>
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_Track_1.pdf">Upper Extremity Trauma - Faculty Profile</a></li>
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_Track_2.pdf">Pelvic Trauma & Hip Fractures - Faculty Profile</a></li>
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_Track_3.pdf">Foot and Ankle Trauma & Spine Trauma - Faculty Profile</a></li>
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_Track_4.pdf">Knee Trauma - Faculty Profile</a></li>
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_Track_5.pdf">Sports Injuries; Arthroscopic Procedures - Faculty Profile</a></li>
                <li><i class="fas fa-file-pdf"></i><a class="hall-res resources_close" href="resources/BOOT_Profiles_Track_6.pdf">Interesting Case Presentations - Faculty Profile</a></li>
            </ul>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="lightbox/html5lightbox.js"></script> -->
    <script src="js/jquery.popupoverlay.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script data-loc="<?php echo $curr_room; ?>" src="js/site.js?v=2"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>
    <script>
        $('#resources').popup({
            transition: 'all 0.5s',
            backgroundactive: true
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-93480057-20');
    </script>

</body>

</html>