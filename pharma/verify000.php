<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
require_once "controls/config.php";

if ((!isset($_GET['e'])) && (!isset($_GET['t']))) {
  header('location:./');
  exit;
}


$errors = [];
$succ = '';
$resend = 0;

$email = mysqli_real_escape_string($link, $_GET['e']);

$sql = "select * from tbl_users where emailid = '$email'";
$res = mysqli_query($link, $sql);

if (mysqli_affected_rows($link) > 0) {
  $data = mysqli_fetch_assoc($res);

  $verified = $data['verified'];
  //echo $verified;

  if (!$verified) {

    if ((count($errors) == 0)) {

      $user_code = mysqli_real_escape_string($link, $_GET['t']);
      $token = $data['token'];

      if ($user_code === $token) {
        $query = "update tbl_users set verified = '1' where emailid = '$email'";
        $qres = mysqli_query($link, $query);
        //echo $query;
        if (mysqli_affected_rows($link) > 0) {
          $succ = 'You have been verified successfully.<br>You can now login to attend BOOT International Live!.';
        } else {
          $errors['database'] = 'You could not be verified at this time. Please try again.';
        }
      } else {
        $errors['invalidcode'] = 'Invalid code. Please try again.';
      }
    }
  } else {
    $succ = 'You have been already verified.';
  }
} else {
  $errors['noemail'] = "Invalid Email ID";
}

?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $event_title; ?></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/styles.css">

<body>
  <div id="preloader">
    <div class="preloader">
      <div class="sk-circle1 sk-child"></div>
      <div class="sk-circle2 sk-child"></div>
      <div class="sk-circle3 sk-child"></div>
      <div class="sk-circle4 sk-child"></div>
      <div class="sk-circle5 sk-child"></div>
      <div class="sk-circle6 sk-child"></div>
      <div class="sk-circle7 sk-child"></div>
      <div class="sk-circle8 sk-child"></div>
      <div class="sk-circle9 sk-child"></div>
      <div class="sk-circle10 sk-child"></div>
      <div class="sk-circle11 sk-child"></div>
      <div class="sk-circle12 sk-child"></div>
    </div>
  </div>


  <div class="container-fluid">
    <div class="row mt-2 mb-2 p-2">
      <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
        <div id="full-height">
          <div id="verification-message">
            <?php
            if (count($errors) > 0) : ?>
              <div class="alert alert-danger">
                <ul class="list-unstyled">
                  <?php foreach ($errors as $error) : ?>
                    <li>
                      <?php echo $error; ?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif;
            ?>
            <?php
            if ($succ != '') {
            ?>
              <div class="alert alert-success">
                <?php
                echo $succ;
                ?>
              </div>
              <a href="./">Continue to Login</a>
              <script>
                /*setTimeout(function(){
                    window.location.href = 'index.php';
                 }, 5000);*/
              </script>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/site.js"></script>




</body>
</head>