<?php
	require_once "../controls/sesAdminCheck.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Users</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="nonverified.php">Non-verified Users</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div>
</nav>

<div class="container-fluid bg-white color-grey">
     
    <div class="row mt-1 p-2">
        <div class="col-12 col-md-6">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/> All Users Download </a>
        </div>
        <div class="col-12 col-md-6">
            <a href="export_live.php"><img src="excel.png" height="45" alt=""/> Online Users Download </a>
        </div>
    </div>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <div id="user-message"></div>
            <div id="users"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getusers', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);
            
        }
    });
    
}

function logoutUser(uid)
{
   $.ajax({
        url: 'ajax.php',
         data: {action: 'logoutuser', userid: uid},
         type: 'post',
         success: function(output) {
             getUsers('1');
         }
   });
}

function updVerify(id)
{
    var verID = '#verify'+id;
    var value = $(verID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'upduserverify', userid :id, ver: value},
        success: function(data){
           // console.log(data);
            if(data == "succ")
            {
                $("#user-message").html('Verification status updated succesfully').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#user-message").html('Verification status could not be updated.').removeClass().addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function updActive(id)
{
    var actID = '#active'+id;
    var value = $(actID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'upduseractive', userid :id, act: value},
        success: function(data){
           // console.log(data);
            if(data == "succ")
            {
                $("#user-message").html('Active status updated succesfully').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#user-message").html('Active status could not be updated.').removeClass().addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function delUser(id)
{
    if(confirm('Are you sure?'))
    {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {action: 'deluser', userId :id},
            success: function(data){
               // console.log(data);
                if(data == "succ")
                {
                    $("#user-message").html('User deleted.').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();
                    getUsers('1');
                }
                else                    
                {
                    $("#user-message").html('User could not be deleted.').removeClass().addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
                }
    
                
            }
        });
    }
    
}
</script>

</body>
</html>