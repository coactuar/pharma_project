<?php
require_once "../controls/config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            $today=date('Y/m/d H:i:s');
            $sql = "SELECT COUNT(id) as count FROM tbl_users where logout_date > '$today'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $loggedin = $row['count'];
            //$loggedin = 0;
        
            
            $sql = "SELECT COUNT(id) as count FROM tbl_users";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
                <div class="col-6">
                    Currently Logged In: <?php echo $loggedin; ?>
                </div>
            </div> 
            <div class="row">
                <div class="col-12">
                    <table class="table table-dark table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Info</th>
                          <th>Topic of Interests</th>
                          <th>Mobile ISP</th>
                          <th>Updates Requested</th>
                          <th>Registered On</th>
                <th width="200">Last Login On</th>
                <th width="200">Last Logged Out  On</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
  //                      $query="select tbl_users.id, tbl_users.first_name, tbl_users.last_name, tbl_users.emailid, tbl_users.phone_num, countries.name as country, states.name as state, cities.name as city, tbl_users.topic_interest, tbl_users.updates, tbl_users.isp, tbl_users.reg_date from tbl_users, cities, states, countries where tbl_users.city=cities.id AND tbl_users.state=states.id AND tbl_users.country=countries.id order by reg_date desc LIMIT $start_from, $limit";
              $query = "select tbl_users.id, tbl_users.first_name, tbl_users.last_name, tbl_users.emailid, tbl_users.phone_num, countries.name as country, states.name as state, cities.name as city, tbl_users.topic_interest, tbl_users.updates, tbl_users.isp, tbl_users.reg_date, tbl_users.login_date, tbl_users.logout_date,tbl_users.current_room from tbl_users, cities, states, countries where tbl_users.city=cities.id AND tbl_users.state=states.id AND tbl_users.country=countries.id ORDER BY tbl_users.logout_date desc LIMIT $start_from, $limit ";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $loggedin = 0;
                        while($data = mysqli_fetch_assoc($res))
                        {
                            $logout = 0;
                        ?>
                          <tr>
                            <td><?php echo $data['first_name'] . ' ' . $data['last_name']; ?></td>
                            <td><?php echo $data['emailid'] . '<br>' .$data['phone_num'] . '<br>' . $data['city'] . ',' . $data['state'] . '<br>' .$data['country']; ?></td>
                            <td><?php 
                            $topics = $data['topic_interest'];
                            $topic = explode(',', $topics);
                            foreach($topic as $t)
                            {
                                switch($t)
                                {
                                    case 'upper-trauma':
                                        echo 'Upper Extremity Trauma'.'<br>';
                                    break;
                                    case 'pelvic-hip':
                                        echo 'Pelvic Trauma &amp; Hip Fractures/Recon'.'<br>';
                                    break;
                                    case 'foot-ankle':
                                        echo 'Foot &amp; Ankle Injuries and Spine Trauma'.'<br>';
                                    break;
                                    case 'knee-trauma':
                                        echo 'Knee Trauma'.'<br>';
                                    break;
                                    case 'sports-injuries':
                                        echo 'Sports Injuries, Arthroscopic Procedures'.'<br>';
                                    break;
                                    case 'presentations':
                                        echo 'Interesting Case Presentations'.'<br>';
                                    break;
                                    
                                }
                            }
                            ?></td>
                            <td><?php echo $data['isp']; ?></td>
                            <td><?php 
                            $updates = $data['updates'];
                            $update = explode(',', $updates);
                            foreach($update as $u)
                            {
                                switch($u)
                                {
                                    case 'program':
                                        echo 'Program'.'<br>';
                                    break;
                                    case 'integrace':
                                        echo 'Integrace Pvt. Ltd.'.'<br>';
                                    break;
                                }
                            } 
                            ?></td>
                            
                            <td><?php 
                                if($data['reg_date'] != ''){
                                    $date=date_create($data['reg_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
<td><?php
                                  if ($data['login_date'] != '') {
                                    $date = date_create($data['login_date']);
                                    echo date_format($date, "M d, H:i a");
                                  } else {
                                    echo '-';
                                  }
                                  ?>
                          </td>
                          <td><?php
                              $today = date("Y/m/d H:i:s");
                    
                              $dateTimestamp1 = strtotime($data['logout_date']);
                              $dateTimestamp2 = strtotime($today);
                              //echo $row[5];
                              if ($dateTimestamp1 > $dateTimestamp2) {
                                /*if ($data['current_room'] != '') {
                                  echo 'Inside ' . $data['current_room'];
                                } else {*/
                                  echo '<i>still logged in</i>';
                               // }
                                //$loggedin += 1; 
                              } else {
                                if ($data['logout_date'] != '') {
                                  $date = date_create($data['logout_date']);
                                  echo 'Logged out on <br>' . date_format($date, "M d, H:i a");
                                } else {
                                  echo '-';
                                }
                                $logout = 1;
                              }
                              ?>
                          </td>                       <!--<td>
                                <select class="input" id="active<?php echo $data['id']; ?>" onChange="updActive('<?php echo $data['id']; ?>')">
                                    <option value="0" <?php if($data['active']=='0') echo 'selected'; ?>>Not Active</option>
                                    <option value="1" <?php if($data['active']=='1') echo 'selected'; ?>>Active</option>
                                </select>
                            </td>-->
                            <td>
                            <a class="btn-delete" href="#" onClick="delUser('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        
        case 'getnonverified':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            $today=date('Y/m/d H:i:s');
            /*$sql = "SELECT COUNT(id) as count FROM tbl_users where logout_date > '$today'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $loggedin = $row['count'];*/
            $loggedin = 0;
        
            
            $sql = "SELECT COUNT(id) as count FROM tbl_users where verified ='0'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row">
                <div class="col-6">
                    Total Non-Verified Users: <?php echo $total_records; ?>
                </div>
                <!--<div class="col-6">
                    Currently Logged In: <?php echo $loggedin; ?>
                </div>-->
            </div> 
            <div class="row">
                <div class="col-12">
                    <table class="table table-dark table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Info</th>
                          <!--<th>Topic of Interests</th>
                          <th>Mobile ISP</th>
                          <th>Updates Requested</th>-->
                          <th width="200">Registered On</th>
                          <th width='100'></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select tbl_users.id, tbl_users.first_name, tbl_users.last_name, tbl_users.emailid, tbl_users.phone_num, countries.name as country, states.name as state, cities.name as city, tbl_users.topic_interest, tbl_users.updates, tbl_users.isp, tbl_users.reg_date from tbl_users, cities, states, countries where tbl_users.city=cities.id AND tbl_users.state=states.id AND tbl_users.country=countries.id AND verified='0' order by reg_date desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $loggedin = 0;
                        while($data = mysqli_fetch_assoc($res))
                        {
                            $logout = 0;
                        ?>
                          <tr>
                            <td><?php echo $data['first_name'] . ' ' . $data['last_name']; ?></td>
                            <td><?php echo $data['emailid'] . '<br>' .$data['phone_num'] . '<br>' . $data['city'] . ',' . $data['state'] . '<br>' .$data['country']; ?></td>
                            <!--<td><?php 
                            $topics = $data['topic_interest'];
                            $topic = explode(',', $topics);
                            foreach($topic as $t)
                            {
                                switch($t)
                                {
                                    case 'upper-trauma':
                                        echo 'Upper Extremity Trauma'.'<br>';
                                    break;
                                    case 'pelvic-hip':
                                        echo 'Pelvic Trauma &amp; Hip Fractures/Recon'.'<br>';
                                    break;
                                    case 'foot-ankle':
                                        echo 'Foot &amp; Ankle Injuries and Spine Trauma'.'<br>';
                                    break;
                                    case 'knee-trauma':
                                        echo 'Knee Trauma'.'<br>';
                                    break;
                                    case 'sports-injuries':
                                        echo 'Sports Injuries, Arthroscopic Procedures'.'<br>';
                                    break;
                                    case 'presentations':
                                        echo 'Interesting Case Presentations'.'<br>';
                                    break;
                                    
                                }
                            }
                            ?></td>
                            <td><?php echo $data['isp']; ?></td>
                            <td><?php 
                            $updates = $data['updates'];
                            $update = explode(',', $updates);
                            foreach($update as $u)
                            {
                                switch($u)
                                {
                                    case 'program':
                                        echo 'Program'.'<br>';
                                    break;
                                    case 'integrace':
                                        echo 'Integrace Pvt. Ltd.'.'<br>';
                                    break;
                                }
                            } 
                            ?></td>-->
                            
                            <td><?php 
                                if($data['reg_date'] != ''){
                                    $date=date_create($data['reg_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
<!--                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php
                                $today=date("Y/m/d H:i:s");
        
                                $dateTimestamp1 = strtotime($data['logout_date']);
                                $dateTimestamp2 = strtotime($today);
                                //echo $row[5];
                                if ($dateTimestamp1 > $dateTimestamp2)
                                {
                                    if($data['current_room'] != ''){
                                        echo 'Inside ' . $data['current_room'];
                                    }
                                    else{
                                        echo '<i>still logged in</i>';
                                    }
                                  //$loggedin += 1; 
                                }
                                else
                                { 
                                  if($data['logout_date'] != ''){
                                      $date=date_create($data['logout_date']);
                                      echo 'Logged out on <br>'. date_format($date,"M d, H:i a"); 
                                      
                                  }
                                  else{
                                      echo '-';
                                  }
                                  $logout = 1;
                                }
                                ?>
                            </td>
-->                         <!--<td>
                                <select class="input" id="active<?php echo $data['id']; ?>" onChange="updActive('<?php echo $data['id']; ?>')">
                                    <option value="0" <?php if($data['active']=='0') echo 'selected'; ?>>Not Active</option>
                                    <option value="1" <?php if($data['active']=='1') echo 'selected'; ?>>Active</option>
                                </select>
                            </td>-->
                            <td>
                            <a class="btn-delete" href="#" onClick="delUser('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'logoutuser':
              $logout_date  = date('Y/m/d H:i:s', time());
              $sql = "update tbl_users set logout_date='$logout_date' where id='".$_POST['userid']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        case 'upduserverify':
              $sql = "update tbl_users set verified='".$_POST['ver']."' where id='".$_POST['userid']."'"; 
              $rs_result = mysqli_query($link,$sql);  
              echo 'succ';
        break;
        case 'upduseractive':
              $sql = "update tbl_users set active='".$_POST['act']."' where id='".$_POST['userid']."'";  
              $rs_result = mysqli_query($link,$sql);  
              echo 'succ';
        break;
        case 'deluser':
              $userId = $_POST['userId'];
              
              $sql = "delete from tbl_users where id = '$userId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;

        case 'getsessions':
            ?>
            <form>
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Session Title</th>
                          <th width="200">Start Time</th>
                          <th width="150">Enable Entry</th>
                          <th width="150">Session Status</th>
                          <th width="100">Show</th>
                          <th width="110"></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_sessions order by start_time asc, session_status asc";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td>
                            <b><?php echo $data['session_title']; ?></b>
                            </td>
                            <td><?php 
                                $date=date_create($data['start_time']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            <td>
                                <select class="input" id="entry<?php echo $data['id']; ?>" onChange="updEntry('<?php echo $data['id']; ?>')">
                                    <option value="0" <?php if($data['launch_status']=='0') echo 'selected'; ?>>No</option>
                                    <option value="1" <?php if($data['launch_status']=='1') echo 'selected'; ?>>Yes</option>
                                </select>
                                
                            </td>
                            <td>
                              <select class="input" id="session<?php echo $data['id']; ?>" onChange="updSession('<?php echo $data['id']; ?>')">
                                    <option value="yet" <?php if($data['session_status']=='yet') echo 'selected'; ?>>Yet to Start</option>
                                    <option value="next" <?php if($data['session_status']=='next') echo 'selected'; ?>>Coming Up Next</option>
                                <option value="live" <?php if($data['session_status']=='live') echo 'selected'; ?>>LIVE Now</option>
                                <option value="over" <?php if($data['session_status']=='over') echo 'selected'; ?>>Completed</option>
                                </select>
                            </td>
                            <td>
                              <select class="input" id="show<?php echo $data['id']; ?>" onChange="updShow('<?php echo $data['id']; ?>')">
                                    <option value="0" <?php if($data['show_session']=='0') echo 'selected'; ?>>No</option>
                                    <option value="1" <?php if($data['show_session']=='1') echo 'selected'; ?>>Yes</option>
                                </select>
                                
                            </td>
                            <td>
                                <a class="btn-edit" href="editsession.php?ses=<?php echo $data['id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delSes('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a>
                                
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                    </tbody>
                    </table>  
                </div>
            </div>   
            </form>
            
            <?php
        
            
        break;
        
        case 'updateentrystatus':
              $newval = $_POST['val'];
              $sesId = $_POST['sesId'];
              
              $sql = "Update tbl_sessions set launch_status ='$newval' where id = '$sesId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;
        
        case 'updatesessionstatus':
              $newval = $_POST['val'];
              $sesId = $_POST['sesId'];
              
              $sql = "Update tbl_sessions set session_status ='$newval' where id = '$sesId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;
        
        case 'updateshowstatus':
              $newval = $_POST['val'];
              $sesId = $_POST['sesId'];
              
              $sql = "Update tbl_sessions set show_session ='$newval' where id = '$sesId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;
        
        case 'delsession':
              $sesId = $_POST['sesId'];
              
              $sql = "delete from tbl_sessions where id = '$sesId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;

        case 'getexhibitors':
            ?>
            <form>
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <!--<th width="120">Logo</th>-->
                          <th>Exhibitor</th>
                          <th width="150">Hall ID</th>
                          <th width="300">Email ID</th>
                          <th width="100">Active</th>
                          <th width="110">Visitors</th>
                          <th width="110"></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_exhibitor_logins order by active desc, exhibitor_name asc";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <!--<td>
                            <img src="../img/exhibs/<?php echo $data['exhibitor_logo']; ?>" width="100" />
                            </td>-->
                            
                            <td>
                            <b><?php echo $data['exhibitor_name']; ?></b>
                            </td>
                            <td><?php echo $data['exhibit_hall_id']; ?></td>
                            <td><?php echo $data['username']; ?></td>
                            <td>
                                <select class="input" id="active<?php echo $data['id']; ?>" onChange="updActive('<?php echo $data['id']; ?>')">
                                    <option value="0" <?php if($data['active']=='0') echo 'selected'; ?>>No</option>
                                    <option value="1" <?php if($data['active']=='1') echo 'selected'; ?>>Yes</option>
                                </select>
                                
                            </td>
                            <td>
                            <?php
                                $exhib_hall_id = $data['exhibit_hall_id'];
            
                                $sql = "SELECT COUNT(*) as count FROM tbl_exhibitor_visitors where exhibit_hall_id = '$exhib_hall_id'";  
                                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $row = mysqli_fetch_assoc($rs_result);
                                $total_visits = $row['count'];
                            //echo $sql;
                                
                                
                                $sql = "SELECT COUNT(DISTINCT attendee_id) as count FROM tbl_exhibitor_visitors where exhibit_hall_id  = '$exhib_hall_id'";  
                                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $row = mysqli_fetch_assoc($rs_result);
                                $total_visitors = $row['count']; 
                                
                                echo '<a href="visitors.php?expid='.$data['id'].'">Visitors:' .$total_visitors .'<br>';
                                echo 'Visits:' .$total_visits .'</a>';
                                
                            ?>
                            </td>
                            <td>
                                <a class="btn-edit" href="editexhibitor.php?exh=<?php echo $data['id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delExhib('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a>
                                
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                    </tbody>
                    </table>  
                </div>
            </div>   
            </form>
            
            <?php
        
            
        break;
        
        case 'updateexhibactive':
              $newval = $_POST['val'];
              $exhId = $_POST['exhId'];
              
              $sql = "Update tbl_exhibitor_logins set active ='$newval' where id = '$exhId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;
        
        case 'delexhib':
              $exhId = $_POST['exhId'];
              
              $sql = "delete from tbl_exhibitor_visitors where exhibit_hall_id in (select exhibit_hall_id from tbl_exhibitor_logins where id = '$exhId')";  
              $rs_result = mysqli_query($link,$sql); 
              
              $sql = "delete from tbl_exhibitors where exhibitor_id = '$exhId'";  
              $rs_result = mysqli_query($link,$sql); 
              
              $sql = "delete from tbl_exhibitor_logins where id = '$exhId'";  
              $rs_result = mysqli_query($link,$sql);  
              //echo $sql;
              echo "succ";
              
        break;

        case 'getresources':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;

            $sql = "SELECT COUNT(id) FROM tbl_exhibitor_resources";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row">
                <div class="col-12">
                    Total Resources: <div class="res_count"><?php echo $total_records; ?></div>
                </div>
                
            </div> 
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Exhibitor</th>
                          <th>Resource Name</th>
                          <th width="110"></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select tbl_exhibitor_resources.id,exhibitor_name, resource_title, download_count, tbl_exhibitor_resources.active from tbl_exhibitor_resources, tbl_exhibitor_logins where tbl_exhibitor_resources.exhib_hall_id = tbl_exhibitor_logins.exhibit_hall_id order by resource_title asc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));

                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td>
                            <?php echo $data['exhibitor_name']; ?>
                            </td>
                            <td>
                            <?php 
                                echo $data['resource_title']; 
                                echo '<br><b>Downloads:</b> '. $data['download_count'];
                            ?>
                            </td>
                            <td><a class="btn-edit" href="editexhibitor.php?exh=<?php echo $data['id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delExhib('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;

        case 'getvideos':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;

            $sql = "SELECT COUNT(id) FROM tbl_exhibitor_videos";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row">
                <div class="col-12">
                    Total Videos: <div class="res_count"><?php echo $total_records; ?></div>
                </div>
                
            </div> 
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Exhibitor</th>
                          <th>Video Title</th>
                          <th width="110"></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select tbl_exhibitor_videos.id, exhibitor_name, video_title, youtube_id, tbl_exhibitor_videos.active from tbl_exhibitor_videos, tbl_exhibitor_logins where tbl_exhibitor_videos.exhib_hall_id=tbl_exhibitor_logins.exhibit_hall_id order by video_title asc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['exhibitor_name']; ?> </td>
                            <td><?php echo $data['video_title']; 
                            echo '<br><b>YouTube ID:</b> '. $data['youtube_id'];
                            ?></td>
                            <td><a class="btn-edit" href="editvideo.php?exh=<?php echo $data['id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delVideo('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="updateVideo(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="updateVideo(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;

        case 'getfeedbacks':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;

            $sql = "SELECT COUNT(id) FROM tbl_conf_feedback";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="150">Name</th>
                          <th width="200">Email ID</th>
                          <th width="150">Experience Rating</th>
                          <th>Comments/Suggestion</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_conf_feedback order by feedback_time desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['name']; ?> </td>
                            <td><?php echo $data['emailid']; ?> </td> 
                            <td><?php echo $data['exp_rating']; ?></td>
                            <td><?php echo nl2br($data['comments']); ?></td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="updateVideo(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="updateVideo(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;

        case 'getvisitors':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            
            $exhib_hall_id = $_POST['exhib_hall_id'];
            
            $sql = "SELECT COUNT(*) as count FROM tbl_exhibitor_visitors where exhibit_hall_id = '$exhib_hall_id'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_visits = $row['count'];
        //echo $sql;
            
            $sql = "SELECT COUNT(DISTINCT attendee_id) as count FROM tbl_exhibitor_visitors where exhibit_hall_id  = '$exhib_hall_id'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_visitors = $row['count']; 
            // echo $sql;
            $total_pages = ceil($total_visitors / $limit);
            ?>
            <div class="row">
                <div class="col-6">
                    Total Visitors: <?php echo $total_visitors; ?>
                </div>
                <div class="col-6">
                    Total Visits: <?php echo $total_visits; ?>
                </div>
            </div> 
            <div class="row">
                <div class="col-12">
                    <table class="table table-dark table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Contact </th>
                          <th>Company</th>
                          <th width="200">Last Visit Time</th>
                          <th width="150">No. of Visits</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select DISTINCT attendee_id from tbl_exhibitor_visitors where exhibit_hall_id = '$exhib_hall_id' LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        //echo $query;
                        $loggedin = 0;
                        while($a = mysqli_fetch_assoc($res))
                        {
                            $u = "select * from tbl_exhibitor_visitors, tbl_users where tbl_exhibitor_visitors.attendee_id = tbl_users.id AND attendee_id='".$a['attendee_id']."' order by entry_time desc limit 1";
                            //echo $u;
                            $r = mysqli_query($link, $u);
                            $data = mysqli_fetch_assoc($r);
                        ?>
                          <tr>
                            <td><?php echo $data['first_name'] . ' ' . $data['last_name']; ?></td>
                            <td><?php echo $data['emailid'].'<br>'. $data['phone_num']; ?></td>
                            <td><?php echo $data['designation'] . ',<br>' .$data['company']; ?></td>
                            <td><?php 
                                if($data['entry_time'] != ''){
                                    $date=date_create($data['entry_time']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                $c = "select count(*) as count from tbl_exhibitor_visitors where attendee_id='".$data['attendee_id']."' and exhibit_hall_id ='$exhib_hall_id'";
                                $r = mysqli_query($link, $c);
                                $d = mysqli_fetch_assoc($r);
                                echo $d['count'];
                                ?>
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;


        
    }
    
}


?>