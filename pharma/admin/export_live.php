<?php
require_once "../controls/config.php";
$today=date('Y/m/d H:i:s');
$sql = "SELECT `first_name`,`last_name`,`emailid`,`phone_num`,countries.name as country, states.name as state,cities.name as city,`topic_interest`,`updates`,`isp`,`reg_date`,`login_date`,`logout_date`  FROM `tbl_users`, countries, states, cities where tbl_users.country=countries.id and tbl_users.state=states.id and tbl_users.city=cities.id and logout_date > '$today' order by logout_date desc";  
$setRec = mysqli_query($link, $sql); 
$columnHeader = '';  
$columnHeader = "#" . "\t". "First Name" . "\t" . "Last Name" . "\t" . "Email ID" . "\t"."Mobile No." . "\t"."Country" . "\t". "State" . "\t"."City" . "\t". "Topics of Interest" . "\t". "Updates Requested" . "\t". "ISP" . "\t". "Registered On" ."\t"."Last Login On" ."\t"."Last Logout On" ."\t";  
$setData = '';  
  $i = 1;
  while ($rec = mysqli_fetch_row($setRec)) {  
    $rowData = '"'.$i.'"' . "\t";  
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
    //echo $rowData.'<br>';
    $i = $i + 1;
}  

  
$file = 'LiveUserlist_' . strtotime($today) . '.xls';  
header("Content-Type: application/octet-stream");  
header("Content-Disposition: attachment; filename=".$file);  
header("Pragma: no-cache");  
header("Expires: 0");  


echo ucwords($columnHeader) . "\n" . $setData . "\n";  

?>