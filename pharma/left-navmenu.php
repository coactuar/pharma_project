<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li> <a class="" href="lobby.php" aria-expanded="false" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a></li>
        <li> <a class="" href="auditorium.php" aria-expanded="false"  title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu"></span>Auditorium</a></li>
        <li> <a class="" href="exhibition-halls.php" aria-expanded="false" title="Go To Exhibition Halls"><i class="fa fa-box-open"></i><span class="hide-menu"></span>Exhibition Halls</a></li>
        <li> <a class="" id="attendee-show" href="#" aria-expanded="false" title="View Attendees"><i class="fa fa-users"></i><span class="hide-menu">Attendees</span></a></li>
        <li> <a class="" id="briefcase-show" href="#" aria-expanded="false" title="My Briefcase"><i class="fas fa-briefcase bc"><div id="chat-message"></div></i><span class="hide-menu">Briefcase</span></a></li>
        <li> <a class=" html5lightbox" href="resources/agenda.pdf" data-width="1200" data-height="1200" data-barheight="0"  title="View Agenda"><i class="fa fa-table"></i><span class="hide-menu"></span>Agenda</a></li>
        <li> <a class="" href="?action=logout" aria-expanded="false" title="Logout"><i class="fa fa-sign-out-alt logout"></i><span class="hide-menu"></span>Logout</a></li>
        
    </ul>
</nav>