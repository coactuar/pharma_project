<?php
//require_once "../controls/config.php";

require_once '../functions.php';

if (isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];

    switch ($action) {



        case 'getteamchatusers':

            $admin = new Admin();
            $userList = $admin->getTeamChatUsers();
            $output = '';
            if (!empty($userList)) {
                $output = '<ul class="chatusers list-unstyled">';
                foreach ($userList as $user) {
                    $user_id = $user['user_id_from'];
                    $chat_time = new DateTime($user['chat_time']);

                    $member = new User();
                    $name = $member->getMemberName($user_id);

                    $unread = $admin->getUnreadChatCount($user_id);

                    $output .= '<li onClick="getTeamChat(\'' . $user['user_id_from'] . '\')">' . $name;
                    if ($unread > 0) {
                        $output .= '<span class="badge badge-danger">' . $unread . '</span>';
                    }
                    $output .= '<span>Last message: ' . date_format($chat_time, "M d Y, H:i:s a") . '</span></li>';
                }
                $output .= '</ul>';
            } else {
                $output = 'No one has sent messages yet.';
            }

            echo $output;

            break;

        case 'getteamchat':

            $user_from = $_POST['user'];
            $user_to = 'team';
            $output = '';

            $member = new User();
            $name = $member->getMemberName($user_from);

            $output = '<div id="chats-heading">
                <h3>' . $name . '</h3>
            </div>';

            $admin = new Admin();
            $chatList = $admin->getTeamChatHistory($user_from, $user_to);

            $output .= '<div id="chat-history" class="scroll">';
            $output .= '<ul class="list-unstyled">';
            foreach ($chatList as $chat) {
                $user_name = '';
                $user_class = '';
                $chat_time = date_create($chat['chat_time']);

                if ($chat['user_id_from'] != 'team') {
                    $user_name = $name;
                    $user_class = 'me';
                } else {
                    $user_name = 'Team Integrace';
                    $user_class = 'team';
                }
                $output .= '<li class="' . $user_class . '"><b>' . $user_name . '</b><span class="time">' . date_format($chat_time, "M d Y, H:i:s a") . '<br>' . $chat['source'] . '</span><div class="msg">';
                $output .= $chat['message'] . '</div></li>';
            }
            $output .= "</ul>";
            $output .= '</div>';

            $admin->updateTeamReadStatus($user_from, $user_to);

            /*$sql = "update tbl_team_chat set read_status = '1' where user_id_to ='$user_to' and user_id_from ='$user_from'";
            $r = mysqli_query($link, $sql);
            */
            echo $output;



            break;

        case 'sendTeamMessage':

            $user_to_id = $_POST["to"];
            $user_from_id = 'team'; //$_POST["from"];
            $message = $_POST["msg"];
            $chat_time   = date('Y/m/d H:i:s');
            $src = '';

            $admin = new Admin();
            $id = $admin->sendTeamMsg($user_to_id, $user_from_id, $message, $src);
            echo $id;
            /*$query="insert into tbl_team_chat(user_id_from, user_id_to, message, chat_time) values('$user_from_id','$user_to_id','$message','$chat_time')";
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
        */
            break;
    }
}
