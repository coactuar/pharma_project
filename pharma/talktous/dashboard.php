<?php
require_once "../controls/sesAdminCheck.php";
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../css/styles.css">
  <link rel="stylesheet" type="text/css" href="../css/admin.css">

</head>

<body class="admin">
  <nav class="navbar navbar-expand-md navbar-dark">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?action=logout">Logout</a>
        </li>
      </ul>

    </div>
  </nav>

  <div class="container-fluid">
    <div id="chat-area">
      <div id="chat-members">
        <div id="member-heading">
          Attendees
        </div>
        <div id="chat-users" class="scroll">

        </div>
      </div>
      <div id="member-chats">
        <div id="member-chat-history"></div>
        <div id="team-chat-form" style="display:none;">
          <form action="#">
            <div class="row">
              <div class="col-10">
                <textarea rows="2" class="form-control" name="chat_message_team" id="chat_message_team"></textarea>
              </div>
              <div class="col-2">
                <button type="button" id="send_teamchat" class="send_teamchat btn-sendmsg btn" data-from='team' data-to='0'>Send</button>
              </div>
            </div>
          </form>
        </div>



      </div>
    </div>

  </div>


  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <?php
  function tosecs($seconds)
  {
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
  }
  ?>
  <script>
    var getUserChat, getUsers;

    function getTeamChatUsers() {
      $.ajax({
        url: 'ajax.php',
        data: {
          action: 'getteamchatusers'
        },
        type: 'post',
        success: function(output) {
          $("#chat-users").html(output);
        }
      });
    }
    getTeamChatUsers();
    getUsers = setInterval(function() {
      getTeamChatUsers();
    }, 6000);

    function getTeamChat(from_user) {
      clearTimeout(getUserChat);
      var from = from_user;
      console.log(from_user);
      $.ajax({
        url: 'ajax.php',
        data: {
          action: 'getteamchat',
          user: from
        },
        type: 'post',
        success: function(output) {
          $("#member-chat-history").html(output);
          $('#send_teamchat').data('to', from_user);
          $('#team-chat-form').css('display', 'block');
          var myDiv = document.getElementById('chat-history');
          myDiv.scrollTop = myDiv.scrollHeight;

          getUserChat = setTimeout(function() {
            getTeamChat(from_user);
          }, 5000);

        }
      });
    }
    $(document).on('click', '.send_teamchat', function() {
      var sendbtn = document.querySelector('.send_teamchat');
      sendbtn.disabled = true;
      var to_user_id = $(this).data('to'); // $(this).data('to');
      var from_user_id = 'team';
      var message = $('#chat_message_team').val();
      if (message.trim() !== '') {
        $.ajax({
          url: 'ajax.php',
          data: {
            action: 'sendTeamMessage',
            to: to_user_id,
            from: from_user_id,
            msg: message
          },
          type: 'post',
          success: function(output) {
            $('#chat_message_team').val('');
            getTeamChat(to_user_id);
          }
        });
      }
      sendbtn.disabled = false;
      return false;
    });
  </script>
</body>

</html>