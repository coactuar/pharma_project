"use strict";
var script = $('script[src*=exhibitor]'); // or better regexp to get the file name..

var exhibId = script.attr('data-to');   
if (typeof exhibId === "undefined" ) {
   exhibId = '0';
}
var newchat, getchat;
$(function(){
    newchat = setInterval(function(){checkfornewexhibchat(exhibId); }, 5000);
    
   $(document).on('click','.exhsendmsg',function(){
       var sendbtn = document. querySelector('.exhsendmsg');
       sendbtn.disabled = true;
       var to_exhibitor_id = $(this).data('to');
       var from_user_id = $(this).data('from');
       var message = $('#exhchatmessage').val();
       if(message.trim() !== '')
       {
           $.ajax({
              url: '../controls/server.php',
              data: {action: 'sendExhMessage', to: to_exhibitor_id, from: from_user_id, msg: message},
              type: 'post',
              success: function() {
                  $('#exhchatmessage').val('');
              }
          });
       }
       sendbtn.disabled = false;
        return false;
  });
  
  $(document).on('click','.delchatmsg',function(){

       var chatmsg_id = $(this).data('id');
      // alert(chatmsg_id);
       if(confirm('Are you sure?'))
       {
           clearInterval(newchat);
           clearInterval(getchat);
            $.ajax({
                url: '../controls/server.php',
                data: {action: 'delExhMessage', msg_id: chatmsg_id},
                type: 'post',
                success: function() {
                    //$('#exhchatmessage').val('');
                    newchat = setInterval(function(){checkfornewexhibchat(exhibId); }, 5000);
                    getExhibChatHistory(exhibId,'exhib-chat-history');  
                    getchat = setInterval(function(){getExhibChatHistory(exhibId,'exhib-chat-history');}, 5000);
    
                }
            });   
       }
    });

    
});

function checkfornewexhibchat(exh_id)
{
    $.ajax({
        url: '../controls/server.php',
        data: {action: 'checknewexhibchat', to: exh_id},
        type: 'post',
        success: function(response) {
            
            if(response>0)
            {
                $('#exchat-message').html('<span class="badge badge-danger">'+response+'</span>').fadeIn();
            }
            else{
                $('#exchat-message').html('').fadeOut();

            }
            
        }
    });   
}

function getExhibChatHistory(to_exhibitor_id, elem){
    $.ajax({
        url: '../controls/server.php',
        data: {action: 'getexhibchathistory', to: to_exhibitor_id},
        type: 'post',
        success: function(response) {
            var tar = '#'+elem;
            //alert(tar);
            $(tar).html(response);
            
        }
    });
}
