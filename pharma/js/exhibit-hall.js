"use strict";

var script = $('script[src*=exhibit-hall]'); // or better regexp to get the file name..

var hallId = script.attr('data-hall');
if (typeof hallId === "undefined") {
    hallId = '';
}

//console.log(hallId);

$(function () {

    $('#req-samples').dialog({
        dialogClass: "requests",
        autoOpen: false,
        title: 'Request For Samples',
        position: { my: "center center", at: "center center" }
    });

    $('#req-products').dialog({
        dialogClass: "requests",
        autoOpen: false,
        title: 'Request For Product Detailing',
        position: { my: "center center", at: "center center" }
    });

    $('#req-literature').dialog({
        dialogClass: "requests",
        autoOpen: false,
        title: 'Request For Literatures',
        position: { my: "center center", at: "center center" }
    });




    $('#video-list').dialog({
        autoOpen: false,
        width: 800,
        height: 400,
        title: 'Videos',
        position: { my: "center center", at: "center center" }
    });

    $('#exhib-resources').dialog({
        autoOpen: false,
        width: 800,
        height: 400,
        title: 'Resources',
        position: { my: "center center", at: "center center" }
    });

    $('#contactus').dialog({
        autoOpen: false,
        width: 800,
        height: 400,
        title: 'Contact Us',
        position: { my: "center center", at: "center center" }
    });

    // $(document).on('click', '.res-videos', function () {
    //     $('#video-list').dialog('open');
    // });
    // $(document).on('click', '.res-resources', function () {
    //     $('#exhib-resources').dialog('open');
    // });
    // $(document).on('click', '.res-contact', function () {
    //     $('#contactus').dialog('open');
    // });


    $('.res-dl').on('click', function () {

        var res_id = $(this).data('docid');
        $.ajax({
            url: 'controls/server.php',
            data: { action: 'updateFileDLCount', resid: res_id },
            type: 'post',
            success: function () {
                //console.log(data);
            }
        });

    });

    $('.vid-view').on('click', function () {
        var vid_id = $(this).data('vidid');
        $.ajax({
            url: 'controls/server.php',
            data: { action: 'updateVideoView', vidid: vid_id },
            type: 'post',
            success: function () {
                //console.log(data);
            }
        });

    });

    updateExh(hallId);
    setInterval(function () { updateExh(hallId); }, 15000);

});

$('#subSampleReq').on('click', function () {
    var exh_id = $(this).data('exhid');
    var user_id = $(this).data('userid');
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'samplereq', exhId: exh_id, userId: user_id },
        type: 'post',
        success: function (output) {
            console.log(output);
            if (output > 0) {
                $('#req-samples').dialog('open');
            }
            else {
                alert('Please try again!');
            }
        }
    });
});

$('#subProductsReq').on('click', function () {
    var exh_id = $(this).data('exhid');
    var user_id = $(this).data('userid');
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'productreq', exhId: exh_id, userId: user_id },
        type: 'post',
        success: function (output) {
            console.log(output);
            if (output > 0) {
                $('#req-products').dialog('open');
            }
            else {
                alert('Please try again!');
            }
        }
    });
});

$('#subLitReq').on('click', function () {
    var exh_id = $(this).data('exhid');
    var user_id = $(this).data('userid');
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'litreq', exhId: exh_id, userId: user_id },
        type: 'post',
        success: function (output) {
            console.log(output);
            if (output > 0) {
                $('#req-literature').dialog('open');
            }
            else {
                alert('Please try again!');
            }
        }
    });
});



function updateExh(hall) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'update-exh', hallId: hall },
        type: 'post',
        success: function (output) {
            //console.log(output);
            if (output === "0") {
                location.href = 'exhibit-halls.php';
            }
        }
    });
}

