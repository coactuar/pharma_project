<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
require_once "model/config.php";
require_once 'model/functions.php';

if ((!isset($_GET['i'])) && (!isset($_GET['t']))) {
  header('location:./');
  exit;
}


$errors = [];
$succ = '';
$resend = 0;

$userid = $_GET['i'];

$member = new User();
$response = $member->isMemberExistsById($userid);

if ($response > 0) {

  $user = $member->getMember($userid);
  //var_dump($user);
  $verified = $user[0]['verified'];
  if (!$verified) {

    if ((count($errors) == 0)) {
      $user_code = $_GET['t'];
      $token = $user[0]['token'];
    }
    if ($user_code === $token) {
      $verified = 1;
      $verify = $member->userVerified($verified, $userid);
      $succ = 'You have been verified successfully.<br>You can now login to attend BOOT International Live!.';
    } else {
      $errors['invalidcode'] = 'Invalid code. Please try again.';
    }
  } //!verified
  else {
    $succ = 'You are already verified.';
  }
} else {
  $errors['noemail'] = "We could not verify you. Please check your email and try again.";
}
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $event_title; ?></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/styles.css">

<body>
  <div id="preloader">
    <div class="preloader">
      <div class="sk-circle1 sk-child"></div>
      <div class="sk-circle2 sk-child"></div>
      <div class="sk-circle3 sk-child"></div>
      <div class="sk-circle4 sk-child"></div>
      <div class="sk-circle5 sk-child"></div>
      <div class="sk-circle6 sk-child"></div>
      <div class="sk-circle7 sk-child"></div>
      <div class="sk-circle8 sk-child"></div>
      <div class="sk-circle9 sk-child"></div>
      <div class="sk-circle10 sk-child"></div>
      <div class="sk-circle11 sk-child"></div>
      <div class="sk-circle12 sk-child"></div>
    </div>
  </div>
  <div class="container">
    <div class="row no-margin">
      <div class="col-12">
        <img src="img/reg-banner.jpg" class="img-fluid" alt="" />
      </div>
    </div>
    <div class="row bg-white color-grey">
      <div class="col-12 text-center">
        <h3 class="reg-title">Verify your Email-ID</h3>
      </div>
    </div>
    <div class="row bg-white color-grey">
      <div class="col-12 col-md-8 offset-md-2">

        <div id="registration-confirmation">
          <?php
          if (count($errors) > 0) : ?>
            <div class="alert alert-danger alert-msg">
              <ul class="list-errors">
                <?php foreach ($errors as $error) : ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif;
          ?>
          <?php
          if ($succ != '') {
          ?>
            <div class="alert alert-success">
              <?php
              echo $succ;
              ?>
            </div>
            <a href="./">Continue to Login</a>
            <script>
              /*setTimeout(function(){
                            window.location.href = 'index.php';
                         }, 5000);*/
            </script>
          <?php
          }
          ?>
        </div>


      </div>
    </div>
    <div class="row bg-white">
      <div class="col-12">
        <img src="img/line-h.jpg" class="img-fluid" alt="" />
      </div>
    </div>
    <div class="row bg-white p-2">
      <div class="col-4 bor-right p-2 text-center">
        <img src="img/in-assoc.png" class="img-fluid bot-img" alt="" />
      </div>
      <div class="col-4 p-2 text-center">
        <img src="img/sci-partner.png" class="img-fluid bot-img" alt="" />
      </div>
      <div class="col-4 bor-left p-2 text-center color-grey">
        <img src="img/brought-by.png" class="img-fluid bot-img" alt="" />
        <div class="visit">
          Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
        </div>
      </div>
    </div>

  </div>

  <div class="container-fluid">
    <div class="row mt-2 mb-2 p-2">
      <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
        <div id="full-height">
          <div id="verification-message">

          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/site.js"></script>




</body>
</head>